package util;

import org.openqa.selenium.WebDriver;

public class LocalDriverFactory {

    private static ThreadLocal<WebDriver> driver=new ThreadLocal<WebDriver>();

    public static void setDriver(WebDriver cDriver){
        driver.set(cDriver);
    }
    public static WebDriver getDriver(){
       return driver.get();
    }
}
