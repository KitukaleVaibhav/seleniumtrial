package com.TestNG_Concepts.testNg;

import org.testng.annotations.Test;

public class InvocationCountConcept {

    /*
    // to use one test case multiple time we use invocationCount=n whatever number we pass that number of times this test case execute
     */
    @Test(invocationCount = 5)
    public void Test1() {
        System.out.println("test 1");
    }

    @Test(invocationCount = 1, threadPoolSize = 1)
    // here we can give thread count to execute this invocation by number of the thread
    public void Test2(String name,String pwd) {
        System.out.println(name+""+pwd);
    }
}
