package com.TestNG_Concepts.testNg;

import org.testng.annotations.*;

public class StructureOfSuitTestClassMethod {

    /*
    @BeforeMethod: This will be executed before every @test annotated method.
@AfterMethod: This will be executed after every @test annotated method.
@BeforeClass: This will be executed before first @Test method execution. It will be executed one only time throughout the test case.
@AfterClass: This will be executed after all test methods in the current class have been run
@BeforeTest: This will be executed before the first @Test annotated method. It can be executed multiple times before the test case.
@AfterTest: A method with this annotation will be executed when all @Test annotated methods complete the execution of those classes inside the <test> tag in the TestNG.xml file.
@BeforeSuite: It will run only once, before all tests in the suite are executed.
@AfterSuite: A method with this annotation will run once after the execution of all tests in the suite is complete.
@BeforeGroups: This method will run before the first test run of that specific group.
@AfterGroups: This method will run after all test methods of that group complete their execution.
     */

    @BeforeSuite
    public void beforeSuite(){

        System.out.println("before suite");
        // we use generally those code here like connection established driver initialization
    }
    @BeforeTest
    public void beforeTest(){

        System.out.println("before Test");
        // this will execute before all classes at once
        /* eg. below code we use in beforeTest Method
System.setProperty("webdriver.chrome.driver", driverPath);
driver = new ChromeDriver();
driver.get(baseUrl);
         */
    }

    @BeforeClass
    public void beforeClass(){

        System.out.println("before Class");
        //this code will work one time only for all method
    }
    @BeforeMethod
    public void beforeMethod(){

        System.out.println("before Method");
        // this will execute before every method prereuisite for test we write here
    }
    @Test
    public void test(){

        System.out.println("test");
    }

    @AfterMethod
    public void afterMethod(){
        System.out.println("after method");
        // this code execute after every method
    }
    @AfterClass
    public void afterClass() {
        System.out.println("after Class");
        // this will execute at once after all method execution
    }
    @AfterTest
    public void afterTest(){
        System.out.println("after Test");
        // this will execute at once after all classes execute
        // driver.close();
    }
    @AfterSuite
    public void afterSuit(){

        System.out.println("after suit");
        // this will work at once after all test execute
    }


}
