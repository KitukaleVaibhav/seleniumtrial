package com.TestNG_Concepts.testNg;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class ParameterizationConcept {
    /*
    Parameterization is attributed as one of the great powers of TestNG. Parameterization in TestNG means we want to pass
    a parameter into the test with multiple values. Each value will run on the same test,
     and the output will be generated for the final analysis. For example, we
     encountered a pin code issue on our hypothetical website. This can be resolved by passing a
     parameter with pin codes. So the step-wise process would look as follows:

Putting the pin code list into the parameters.
Passing the parameter to the tests.
Check whether we get the correct city name by the pin code or not.
Generating results.

Why do we Need to Parameterize Tests in TestNG?
When we parameterize tests in TestNG, we create a pipeline of automation testing. In this pipeline,
 we feed values from one end while generating results from another. However, we create this pipeline only once.
 For example, if I want to pass many pin codes for my tests, I can skip the efforts such as:

if (pin_code == 249403)

  {

      Do something

}

else if (pin_code == 110016)

{

Do something

}

else if (pin_code == 500012)

{

Do something

}

…….and so on.

Instead, I can reconstruct this as follows:

@parameter_value

Hit API to test pin_code

Generate result

Later, when we add or delete the pin code values,
we just need to delete them from the list we were feeding into the pipeline. This process saves a lot of time, effort, and work.
     */

    @Test
    @Parameters({"Name", "Surname"})
    // note we passed this parameter name here and the value we get from xml file at runtime.
    public void Personaldetails(String name, String surname) {
        System.out.println("Name: " + name);
        System.out.println("Surname: " + surname);
    }

    @Test
    @Parameters({"employeeId", "designation"})
    public void employmentDetails(int employee_Id, String Designation) {
        System.out.println("employeeId :" + employee_Id + "    " + "Designation :" + Designation);
        if (employee_Id < 100) {
            System.out.println("employee is eligible for reward offer");
        } else {
            System.out.println("employee is not eligible for reward offer");
        }
    }

}
