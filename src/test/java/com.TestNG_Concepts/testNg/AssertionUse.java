package com.TestNG_Concepts.testNg;

import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class AssertionUse {

    @Test(priority = 1)
    public void HardAssert() {
        int a = 10;
        int b = 20;

        Assert.assertEquals(a, b);
        /*
         note: in Hard Asser on line where the assertion failed it will throw exception and remaining code of
          this method will not execute those code will be unreachable. yes but next method will execute.
          Hard assert has a class Assert and Assert class contains mulitple assert methods.
         */
        System.out.println("hard assertion");
    }

    @Test(priority = 2)
    public void softAssert() {
        int a = 10;
        int b = 20;

        SoftAssert assertion = new SoftAssert();
        assertion.assertEquals(a, b);
        /*
        note: in soft assertion if your Assertion failed even it not throw exception immidiately it will execute
        remaining code of the test and after all execcution it will throw exception
        for soft assertion we have class SoftAssert
        in soft Assert we can decide when you wan't throw exception
         */


        System.out.println("soft assertion assertion");
        assertion.assertAll();
        /*this method is mandatory in SoftAssertion to throw exception and we can use this line at any location in this code
        after which line you want to throw exception generally we wright this line at end of the code so that all code should execute
        and at once all exception should throw.*/
    }
}
