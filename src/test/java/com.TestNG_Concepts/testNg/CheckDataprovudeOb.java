package com.TestNG_Concepts.testNg;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CheckDataprovudeOb {
    int row = 4;
    int column = 2;

    public static void travers() {
        for (int a = 0; a < 4; a++) {
            for (int b = 0; b < 2; b++) {
                System.out.println("a:" + a + " b: " + b);
            }
        }
    }

    @Test(dataProvider = "profileData")
    public static void dataTest(String name, String surname, int age) {
        System.out.println("uname:" + name + " surname:" + surname + " age:" + age);
    }

    @DataProvider(name = "profileData")
    public Object[][] provider() {

        Object info[][] = {{"raj", "patil", 90}, {"mahesh", "kitukale", 19}, {"santosh", "kitukale", 27}};
        return info;
    }
}
