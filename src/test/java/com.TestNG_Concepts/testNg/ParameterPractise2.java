package com.TestNG_Concepts.testNg;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class ParameterPractise2 {

    @Test
    public void personalInfo(String name,String surname){
        System.out.println(name+" "+surname);
    }
    @Test
    @Parameters({"companyName","id"})
    public void professionalInfo(String comapnyName,int id){
        System.out.println(comapnyName+" "+id);
    }
}
