package com.TestNG_Concepts.testNg;

import org.testng.annotations.Test;
/*
note:- ascii value of (A-Z) is (65-90) and (a-z) is (97-122)
if you not set the priority with test cases then it will execute test cases as per ascii value of method name lowest ascii value
method execute first.
if we set same priority with method then also first it will check priority if this same then it will execute as per ascii value.
 */

public class Prioritization {

    @Test(priority = 1)
    public static void anter() {
        System.out.println("anter");
    }

    @Test(priority = 2)
    public static void abir() {
        System.out.println("abir");
    }

    @Test(priority = 4)
    public static void test3() {
        System.out.println("test3");
    }

    @Test(priority = 3)
    public static void test4() {
        System.out.println("test4");
    }

    @Test(priority = 5)
    public static void Checking() {
        System.out.println("Checking");
    }

    @Test(priority = 6)
    public static void Order() {
        System.out.println("Order");
    }

    @Test(priority = 7)
    public static void Oil() {
        System.out.println("Oil");
    }

}
