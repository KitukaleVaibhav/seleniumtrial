package com.TestNG_Concepts.testNg;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class RunOnlyFailedTestCase implements IRetryAnalyzer {

    private int retryCount = 0;
    private static final int maxRetryCount = 3;


    @Override
    public boolean retry(ITestResult iTestResult) {
            if (retryCount < maxRetryCount) {
                retryCount++;
                System.out.println(iTestResult.getMethod().getMethodName());
                return true;

            }
            return false;
        }
    }



