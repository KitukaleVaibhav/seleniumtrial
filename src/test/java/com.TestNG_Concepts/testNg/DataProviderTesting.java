package com.TestNG_Concepts.testNg;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;

public class DataProviderTesting {

    static WebDriver driver;

    @BeforeMethod
    public void beforeMethod() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();


    }


    @Test(dataProvider = "signindata")
    public static void dataProvider(String username, String password) {
        driver.get("https://www.amazon.in/");
        driver.findElement(By.xpath("//span[text()='Hello, sign in']")).click();
        driver.findElement(By.xpath("//input[@type='email']")).sendKeys(username);
        driver.findElement(By.xpath("//input[@id='continue']")).submit();
        driver.findElement(By.xpath("//input[@type='password']")).sendKeys(password);
        driver.findElement(By.xpath("//input[@id='signInSubmit']")).submit();
    }

    @Test
    public static void check2() {
        driver.get("https://gitlab.com/");
    }

    @Test
    public static void check3() {
        driver.get("https://www.amazon.in/");
    }


    @DataProvider(name = "signindata")
    public Object[][] logindate() {

        Object[][] data = {{"8888959054", "sdnsjn"}, {"88887759054", "sdnsjn@45"}, {"847959054", "sdn"}, {"9209022720", "amazon@12390899"}};
        return data;
    }

    //  @Test(dataProvider = "Practise")
    public void data2(String name, String pass, String add) {
        System.out.println(name + " " + pass + " " + add);
    }

    @DataProvider(name = "Practise")
    public Object[][] pract() {
        Object[][] Data2 = {{"8888959054", "sdnsjn", "amravati"}, {"88887759054", "sdnsjn@45", "Pune"}, {"847959054", "sdn", "bangloe"}, {"9209022720", "amazon@123890909", "chandur"}};
        return Data2;
    }

    @DataProvider(name = "MultiValue")
    public static Object[][] TestData() {
/* 4 is row 2 is column note 4 row means: 0,1,2,3 and 2 coloumn means: 0,1 indexing always start from 0 because it's array index
we can use any number of rows and coloumn there is no restriction for number of rows and coloumn in dataProvider
    */
        Object[][] logdata = new Object[4][2];
        logdata[0][0] = "admin";
        logdata[0][1] = "manager";

        logdata[1][0] = "admin";
        logdata[1][1] = "manaaager";

        logdata[2][0] = "admin123";
        logdata[2][1] = "manager";

        logdata[3][0] = "adimnnnn";
        logdata[3][1] = "man@344";

        return logdata;
    }


    // @Test(dataProvider = "TestData3")
    public void Test3(String username, String password) {
        System.out.println(username + " " + password);
    }

    @DataProvider()
    public static Object[][] TestData3() {
        Object[][] logdata = new Object[4][2];
        logdata[0][0] = "admin";
        logdata[0][1] = "manager";

        logdata[1][0] = "admin";
        logdata[1][1] = "manaaager";

        logdata[2][0] = "admin123";
        logdata[2][1] = "manager";

        logdata[3][0] = "adimnnnn";
        logdata[3][1] = "man@344";
        return logdata;
    }

   // @AfterMethod
    public void teardown() {
        driver.quit();
    }
}
