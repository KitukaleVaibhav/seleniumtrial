package com.TestNG_Concepts.testNg;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners(ListnerPractise.class)  // mostly we use Listner directly in our .xml file so that it will execute for all classes in xml file
public class ListnerCalledOnTest {

    @Test
    public void test1(){
        System.out.println("test1");
        int a = 10;
        int b = 20;

        Assert.assertEquals(a, b);
    }
    @Test
    public void test2(){
        System.out.println("test2");

    }
    @Test
    public void test3(){
        System.out.println("test3");
    }
}
