package com.TestNG_Concepts.testNg;

import org.testng.annotations.Test;

public class GroupingOfTest {

    @Test(groups = {"smoke", "sanity"})
    public void test1() {
        System.out.println("test 1: smoke ,sanity");
    }

    @Test(groups = {"regression", "smoke"})
    public void test2() {
        System.out.println("test 2: regression smoke");
    }

    @Test(groups = "smoke")
    public void test3() {
        System.out.println("test 3: smoke");
    }

    @Test(groups = {"smoke", "sanity"})
    public void test4() {
        System.out.println("test 4: smoke sanity");
    }

    @Test(groups = "smoke")
    public void test5() {
        System.out.println("test 5: smoke");
    }

    @Test(groups = {"smoke", "sanity"})
    public void test6() {
        System.out.println("test 6: smoke, sanity");
    }

    @Test(groups = "regression")
    public void test7() {
        System.out.println("test 7: Regression");
    }

    @Test(groups = {"regression", "smoke"})
    public void test8() {
        System.out.println("test 8: Regresion, smoke");
    }
}
