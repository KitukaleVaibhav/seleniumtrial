package com.TestNG_Concepts.testNg;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.io.File;
import java.io.IOException;

public class ListnerPractise implements ITestListener {
    /*
    Listners are component in testNG That Listnes (keep track of) of execution and helps to perform
    action at multiple stages of test execution.
    OR
    Lisnter it is an interface that is listening to the event that are performed by testing
     */

    @Override
    public void onTestStart(ITestResult result) { //  it will call before each test method execution
        System.out.println("onTestStart");
    }

    @Override
    public void onTestSuccess(ITestResult result) { //it will call after each success test method.
       System.out.println("onTestSuccess");
    }

    @Override
    public void onTestFailure(ITestResult result) { //it will call after each failed test method.
        System.out.println("onTestFailure");
    }

    @Override
    public void onTestSkipped(ITestResult result) { //it will call after each skipped test method.
        System.out.println("onTestSkipped");
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        System.out.println("onTestFailedButWithinSuccessPercentage");
    }

    @Override
    public void onStart(ITestContext context) { // this will execute only one time of execution
        System.out.println("onStart");
    }

    @Override
    public void onFinish(ITestContext context) { // this will execute at once after all execution happen
        System.out.println("onFinish");
    }
}
