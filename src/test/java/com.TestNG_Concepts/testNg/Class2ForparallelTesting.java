package com.TestNG_Concepts.testNg;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.Duration;

public class Class2ForparallelTesting {
    /*
    note: what parallel execution of classes happen if one thread execute the method of one class and parallaly second thread execute method
    of second class so that we use parallel execution. if we not use this then execution of class two will start when the excution of class
    one is end.
     */

    @Test
    public void openAmazon() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.get("https://www.amazon.in/");
        int a = 10;
        int b = 50;

       Assert.assertEquals(10, 50);
    }
}
