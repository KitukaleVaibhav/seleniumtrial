package com.TestNG_Concepts.testNg;

import org.testng.annotations.Test;

public class MethodEnableDisbaled {

    @Test                     // note by default the method is enabed
    public void EnabledMethod1() {
        System.out.println("enabled method 1");
    }

    @Test(enabled = false)
    // when we use enabled=false parameter then this method will disabled and it will ot execute at runtime.
    public void disabledMethod() {
        System.out.println("disbaled method");
    }

    @Test(enabled = true)
    // we can use enabled=true to enabled method but it is by default configuration with metho
    public void EnabledMethod2() {
        System.out.println("enabled method2");
    }
}
