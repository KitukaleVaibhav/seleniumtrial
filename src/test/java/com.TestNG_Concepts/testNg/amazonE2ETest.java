package com.TestNG_Concepts.testNg;

import allureListener.BaseClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class amazonE2ETest {

    public WebDriver driver;

    @Test
    @Parameters("Url")
    public void placeOrder(String url) {
        BaseClass bs = new BaseClass();
        driver = bs.initialize_driver();// this method return WebDriver instance
        driver.get(url);// this method use to load url in browser
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.findElement(By.xpath("//a[normalize-space()='Electronics']")).click();
        driver.findElement(By.xpath("//span[normalize-space()='Laptops & Accessories']")).click();
        driver.findElement(By.xpath("//span[@dir='auto'][normalize-space()='Laptops']")).click();
        driver.findElement(By.xpath("label[for='apb-browse-refinements-checkbox_4']")).click();
        driver.findElement(By.xpath("//img[@alt='Sponsored Ad - Lenovo IdeaPad Slim 3 Intel Core i3 11th Gen 14\" (35.56cm) FHD Thin & Light Laptop (8GB/512GB SSD/Windows 1...']")).click();

        Set<String> windowId = driver.getWindowHandles();
        List<String> wins = new ArrayList<String>(windowId);
        String winodw1 = wins.get(1);
        driver.switchTo().window(winodw1);

        driver.findElement(By.xpath("//input[@id='buy-now-button']")).click();
        driver.findElement(By.xpath("//input[@id='ap_email']")).sendKeys("9209022720");
        driver.findElement(By.xpath("//input[@id='continue']")).click();
        driver.findElement(By.xpath("//input[@id='ap_password']")).sendKeys("Amazon@123");

    }
}
