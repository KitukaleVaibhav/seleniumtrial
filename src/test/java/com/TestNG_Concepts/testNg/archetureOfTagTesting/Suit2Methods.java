package com.TestNG_Concepts.testNg.archetureOfTagTesting;

import org.testng.annotations.Test;

public class Suit2Methods {

    @Test(priority = 1)
    public static void test1() {
        System.out.println("test1 suite2 method1");
    }

    @Test(priority = 2)
    public static void test2() {
        System.out.println("test2 suite2 method2");
    }

    @Test(priority = 3)
    public static void test3() {
        System.out.println("test3 suite2 method3");
    }

    @Test(priority = 4)
    public static void test4() {
        System.out.println("test4 suite2 method 4");
    }

    @Test(priority = 5)
    public static void test5() {
        System.out.println("test5 suite2 method 5");
    }
}
