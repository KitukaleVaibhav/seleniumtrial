package SeleniumBasic.Locators;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class ShadowDOMElement {

    public static void main(String[] args) {
        ChromeOptions options=new ChromeOptions();
        options.setAcceptInsecureCerts(true);
        WebDriverManager.chromedriver().setup();
        WebDriver driver=new ChromeDriver(options);

        driver.manage().window().maximize();
        driver.get("https://books-pwakit.appspot.com/");
       WebElement Root= driver.findElement(By.tagName("book-app"));// shadowHost or root element

        JavascriptExecutor js=(JavascriptExecutor)driver;
      // WebElement ShadowDom1=(WebElement) js.executeScript("return arguments[0].shadowRoot", ShadowRoot);
        SearchContext ShadowDom1= (SearchContext)js.executeScript("return arguments[0].shadowRoot",Root);
WebElement appHeader=ShadowDom1.findElement(By.tagName("app-header"));
WebElement appToolbar=appHeader.findElement(By.cssSelector("app-toolbar.toolbar-bottom"));
WebElement bookDecorator=appToolbar.findElement(By.tagName("book-input-decorator"));
bookDecorator.findElement(By.cssSelector("input#input")).sendKeys("automatio");


//        WebElement shadowHost = seleniumWebDriver.findElement(By.cssSelector("#shadowrootcontainer"));
//        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) seleniumWebDriver;
//        SearchContext shadowRoot = (SearchContext) javascriptExecutor.executeScript("return arguments[0].shadowRoot", shadowHost);
//        WebElement shadowContent = shadowRoot.findElement(By.cssSelector("#shadowcontentcss"));
    }
}
