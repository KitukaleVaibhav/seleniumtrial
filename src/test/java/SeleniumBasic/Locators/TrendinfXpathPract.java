package SeleniumBasic.Locators;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

public class TrendinfXpathPract {
    public static void main(String[] args) {
        WebDriverManager.chromedriver().setup();
        WebDriver driver=new ChromeDriver();
driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.manage().window().maximize();
        driver.get("https://www.youtube.com/feed/trending");
        driver.findElement(By.xpath("(//ytd-video-renderer[1]//div[1]//ytd-thumbnail[1]//a[1]//yt-image[1]//img[1])[1]")).click();
    }
}
