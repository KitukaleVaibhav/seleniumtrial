package SeleniumBasic.Locators;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

public class LocatorUse {
    public static void main(String[] args) {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
        driver.get("https://groww.in/");
        WebElement loginButton = driver.findElement(By.xpath("//div[@class='btn96DefaultClass absolute-center cur-po" +
                " bodyBaseHeavy contentPrimary cur-po btn96MediumButton btn96ButtonLabel" +
                " contentOnColour backgroundAccent btn96ButtonHover']"));

        loginButton.click();
        WebElement usernameField = driver.findElement(By.id("login_email1"));
        usernameField.sendKeys("");
        WebElement continueBtn = driver.findElement(By.xpath("(//div[contains(@class,'btn96DefaultClass absolute-center')])[1]"));
        continueBtn.click();
        WebElement passwordField = driver.findElement(By.id("login_password1"));
        passwordField.sendKeys("");
        WebElement finalLoginButton = driver.findElement(By.xpath("(//div[contains(@class,'btn96DefaultClass')])[1]"));
        finalLoginButton.click();
          /*
           try{
            for(int i=0;i>=-1;i++) {
                WebElement marketPercent = driver.findElement(By.xpath("(//div[@class='mvc768CurrVal bodyLargeHeavy'])[1]"));
                String marketValue = marketPercent.getText();
                System.out.println(marketValue);
                if (marketValue.contains("2.90")||marketValue.contains("2.92")||marketValue.contains("2.94")) {
                    System.out.println("target Match");
                    break;
                }
            }
        }catch (Exception e){
            driver.navigate().refresh();
        }
*/
        //1 st Locator id
//       WebElement addresBox= driver.findElement(By.id("nav-global-location-slot"));
//       System.out.println("addres size : "+addresBox.getSize());

       /*// 2nd locator name
       WebElement searchbox= driver.findElement(By.name("field-keywords"));
        System.out.println("searchbox displayed : "+searchbox.isDisplayed());
        // 3rd locator linkText
       WebElement mobile= driver.findElement(By.linkText("Mobiles"));
        System.out.println("mobileLink displayed : "+mobile.isDisplayed());
        mobile.click();

        // 4 th locator partialLinkText
        WebElement AmazonminiTV=driver.findElement(By.partialLinkText("Amazon mini"));// here you can passed partial link text
        System.out.println("AmazonminiTV displayed : "+AmazonminiTV.isDisplayed());
        AmazonminiTV.click();

        // 5th locator classname
       WebElement amazon= driver.findElement(By.className("AppNavbar_mShopIcon__4UsHr"));
        System.out.println("Amazon homePage : "+amazon.isDisplayed());
        amazon.click();


        // 6 th locator tagName
      List<WebElement> Linksize=driver.findElements(By.tagName("a"));
       System.out.println("link size :"+Linksize.size());
        driver.quit();
        */
// Capture or find SVG WebElement
     /*   driver.get("https://www.javatpoint.com/");
      WebElement svg= driver.findElement(By.xpath("(//*[local-name()='svg' and @viewBox='0 0 13 13'])[1]"));
      System.out.println("svg element size:"+svg.getSize());
      */

    }
}

