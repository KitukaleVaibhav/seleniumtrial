package SeleniumBasic.Locators;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class XpathAxesHandeling {
    public static void main(String[] args) {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();

        driver.manage().window().maximize();
        driver.get("https://money.rediff.com/gainers");

        // for self      //tagName[@attName='attValue']/self::tagName it's a mandatory
        WebElement self = driver.findElement(By.xpath("//a[contains(text(),'Innovative Ideals')]/self::a"));
        System.out.println("self node :" + self.isDisplayed());

        // for navigate from self to parent //tagName[@attName='attValue']/parent::tagName
        WebElement parent = driver.findElement(By.xpath("//a[contains(text(),'Innovative Ideals')]/parent::td"));
        System.out.println("parent node :" + parent.isDisplayed());

        // for navigate self to ancestor //tagName[@attName='attValue']/ancestor::tagName
        WebElement ancestor = driver.findElement(By.xpath("//a[contains(text(),'Innovative Ideals')]/ancestor::tr"));
        System.out.println("ancestor node :" + ancestor.isDisplayed());

        // for navigate self to child //tagName[@attName='attValue']/child::tagName
        WebElement child = driver.findElement(By.xpath("//a[contains(text(),'Innovative Ideals')]/ancestor::tr/child::td"));
        System.out.println("child node :" + child.isDisplayed());

        // for navigate self to descendant //tagName[@attName='attValue']/descendant::tagName
        WebElement descendant = driver.findElement(By.xpath("//a[contains(text(),'Innovative Ideals')]/ancestor::tr/descendant::td[1]"));
        System.out.println("descendant node :" + descendant.isDisplayed());

        // for navigate self to following node //tagName[@attName='attValue']/following::tagName
        int followingElement = driver.findElements(By.xpath("//a[contains(text(),'Innovative Ideals')]/ancestor::tr/following::tr")).size();
        System.out.println("Count of following Element  :" + followingElement);// note following element means all element come after the self node means element which is below self element in DOM.

        // for navigate self to following sibling //tagName[@attName='attValue']/following-sibling::tagName
        int followingSibling = driver.findElements(By.xpath("//a[contains(text(),'Innovative Ideals')]/ancestor::tr/following-sibling::tr")).size();// following-sibling means only those element which is after self element in DOM and those element node is same.
        System.out.println("followingSibling count  :" + followingSibling);

        // for navigate self to preceding //tagName[@attName='attValue']/preceding::tagName
        int preceding = driver.findElements(By.xpath("//a[contains(text(),'Innovative Ideals')]/ancestor::tr/preceding::tr")).size();// preceding means all above/previous element of self element in DOM.
        System.out.println("preceding count  :" + preceding);

        // for navigate self to preceding-sibling //tagName[@attName='attValue']/preceding-sibling::tagName
        int precedingSibling = driver.findElements(By.xpath("//a[contains(text(),'Innovative Ideals')]/ancestor::tr/preceding-sibling::tr")).size();// preceding sibling means only those above or previous element of self element which is at sibling or in same node
        System.out.println("precedingSibling count  :" + precedingSibling);

    }
}
