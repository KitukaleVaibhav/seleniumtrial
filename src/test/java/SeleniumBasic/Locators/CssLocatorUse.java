package SeleniumBasic.Locators;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class CssLocatorUse {
    public static void main(String[] args) {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();

        driver.manage().window().maximize();
        driver.get("https://www.amazon.in/");


        // 7 th locator CSS(Cascading style sheet) selector

        // General syntax of Css:-  tagname[attributeName='attributeValue']
// tag and attribute combination
        WebElement css1 = driver.findElement(By.cssSelector("a[data-csa-c-content-id='nav_cs_bestsellers']"));
        System.out.println("css1 displayed :" + css1.isDisplayed());

// type 2 tagname[attributName1='attributeValue1'][attributeName2='attributeValue2] we use this syntax if single attribute matches with multiple element
        WebElement locationSymbol = driver.findElement(By.cssSelector("div[class='nav-sprite nav-progressive-attribute'][id='nav-packard-glow-loc-icon']"));
        System.out.println("location symbol displayed :" + locationSymbol.isDisplayed());

// type 3 tagname[attributeName*='attributeValue']// note this * represent contains means here in value you can use partial tesxt also to loacte webelement
        WebElement css4 = driver.findElement(By.cssSelector("a[id*='skip']"));// we use this modified path if value change but it contains some value
        System.out.println("css4 displayed :" + css4.isDisplayed());

// type 4 tagName[attributeName$='attributeValue']// $ represent attribute value endswith
        WebElement css5 = driver.findElement(By.cssSelector("a[class$='progressive-attribute']"));// we use this modofied css value if value chages from start but it end value will be constant
        System.out.println("css5 displayed :" + css5.isDisplayed());

        //type 5 tagName[attributeName^='attributeValue']// ^ repersent attributeValue start with
        WebElement css6 = driver.findElement(By.cssSelector("a[class^='nav-logo']"));// we use this modified css path if our value change from end but it start value constant
        System.out.println("css6 displayed :" + css6.isDisplayed());
// note: in css selector # denoted id and . denote class

        // Tag and Id combination
        WebElement css2 = driver.findElement(By.cssSelector("a#nav-logo-sprites"));//note here a is tagname and nav-logo-sprites is a id and # is a sepataror
        System.out.println("css2 displayed :" + css2.isDisplayed());
        // Tag and class combination

        WebElement css3 = driver.findElement(By.cssSelector("a.skip-link"));// here tag and class we separate with(.) that is represnt class
        System.out.println("css3 displayed :" + css3.isDisplayed());

        // travel toward child element a[class^='nav-logo'] > span:first-child // you can use last-child also this is a standard synatax to travers child element we use > sign
        WebElement css7 = driver.findElement(By.cssSelector("a[class^='nav-logo'] > span:first-child"));
        System.out.println("css7 displayed :" + css7.isDisplayed());
    }
}
