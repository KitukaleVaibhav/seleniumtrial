package SeleniumBasic.Locators;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class XpathLocatorUse {
    /*
    note: Xpath is defined as XML path.
    its is a syntax or language to find any element on web page using XML expression.
    Xpath use to find location of webelement on webpage using HTML DOM structure.
    Xpath can be use to navigate through elements and Attribute in DOM.

Absolute Xpath: absoloute xpath start from root node.
   Relative Xpath: relative xpath directly jump to element in DOM.
     */

    public static void main(String[] args) {

        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();

        driver.manage().window().maximize();
        driver.get("https://www.amazon.in/");

/*
Syntax of Relative Xpath1: //tagname[@attributeName='attributeValue']
 */
        WebElement mobile = driver.findElement(By.xpath("//a[@href='/mobile-phones/b/?ie=UTF8&node=1389401031&ref_=nav_cs_mobiles']"));
        System.out.println("Mobile displayed :" + mobile.isDisplayed());

        // syntax 2 Xpath with Or Operartor: //tagname[@attName1='attvalue1' or @attName2='attValue2']  we use this xpath when atleast one of two should work
        WebElement mobile2 = driver.findElement(By.xpath("//a[@href='/mobile-phones/b/?ie=UTF8&node=1389401031&ref_=nav_cs_mobiles' or @data-csa-c-content-id='nav_cs_mobiles']"));
        System.out.println("Mobile2 displayed :" + mobile2.isDisplayed());

// syntax 3 Xpath with and Operartor: //tagname[@attName1='attvalue1' and @attName2='attValue2']  we use this xpath and both value from xpath should be exist then it will return webelement
        WebElement mobile3 = driver.findElement(By.xpath("//a[@href='/mobile-phones/b/?ie=UTF8&node=1389401031&ref_=nav_cs_mobiles' and @data-csa-c-content-id='nav_cs_mobiles']"));
        System.out.println("Mobile3 displayed :" + mobile3.isDisplayed());

// Syntax 4 Xpath with contains() method  //tagname[contains(@attName,'attvalue')]
        WebElement mobile4 = driver.findElement(By.xpath("//a[contains(@href, '/mobile-phones/b/?ie=')]"));// we use this if attribute value dynamic but it's some value always constant
        System.out.println("Mobile4 displayed :" + mobile4.isDisplayed());

        // syntax 5 Xpath with start-with() //tagnama[starts-with(@attName,'attvale']
        WebElement mobile5 = driver.findElement(By.xpath("//a[starts-with(@href, '/mobile-phones/b/?ie=')]"));// we use this method if the attribute value change from end but constant fron start
        System.out.println("Mobile5 displayed :" + mobile5.isDisplayed());

        // syntax 6 xpath with text() method //tagName[text()='mobile']
        WebElement mobile6 = driver.findElement(By.xpath("//a[text()='Mobiles']"));
        System.out.println("Mobile6 displayed :" + mobile6.isDisplayed());

        // syntax 7 x path with end-with method
        // WebElement mobile7 = driver.findElement(By.xpath("//input[ends-with(@id,'_PHONE$1']"));

        // syntax 8 chain xpath //tagname1[@attName='attVal']//tagName2
        WebElement eight = driver.findElement(By.xpath("//span[@class='nav-line-2']//span[@class='nav-icon nav-arrow'])"));
        System.out.println("eight displayed :" + eight.isDisplayed());


    }
}