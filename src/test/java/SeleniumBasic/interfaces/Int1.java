package SeleniumBasic.interfaces;

public interface Int1 {
    void add();
    Int2 sub();

    public interface Int2{
        void div();
        void mult();
    }
}
