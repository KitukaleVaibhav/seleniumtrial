package SeleniumBasic.methods;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;
import java.util.Set;

public class LinkOpenHandeling {
    static WebDriver driver;

    public static void main(String[] args) {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        //openLinkInNewTab1();
        //openLinkInNewWindow();
        openLinkInNewTab2();
    }

    public static void openLinkInNewTab1() {
        driver.get("https://www.selenium.dev/");
        WebElement downloadLink = driver.findElement(By.xpath("//a[@href='/downloads']"));// link which you wan't to open
        String tab = Keys.chord(Keys.CONTROL, Keys.RETURN);
        // this method open the link in new tab using in same window and the link is of same application
        downloadLink.sendKeys(tab);
    }

    public static void openLinkInNewTab2() {
        /*
        if you wan't to open new url in new tab then you need to use below combination
         */
        driver.get("https://www.selenium.dev/");
        driver.switchTo().newWindow(WindowType.TAB);
        driver.get("https://www.imdb.com/");
        // this method use to oepn url in new tab in same window note: both url is different application


        Set<String> windows = driver.getWindowHandles();
        for (String window : windows) {
            String title = driver.switchTo().window(window).getTitle();
            System.out.println("title :" + title);
        }
    }

    public static void openLinkInNewWindow() {
        driver.get("https://www.selenium.dev/");
        driver.switchTo().newWindow(WindowType.WINDOW);// this method use to open url in different window
        driver.get("https://www.imdb.com/");
    }
}
