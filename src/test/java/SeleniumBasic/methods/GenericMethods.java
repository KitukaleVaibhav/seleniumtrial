package SeleniumBasic.methods;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.Set;

public class GenericMethods {


    public static void switchWindow(Set<String> element, WebDriver driver, String title) {
        for (String windowID : element) {
            String Actualtitle = driver.switchTo().window(windowID).getTitle();
            if (Actualtitle.contains(title)) {
                driver.switchTo().window(windowID);
            }
        }

    }
}
