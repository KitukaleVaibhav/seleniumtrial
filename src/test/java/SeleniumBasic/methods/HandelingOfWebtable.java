package SeleniumBasic.methods;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;
import java.util.List;

public class HandelingOfWebtable {
    static WebDriver driver;

    public static void main(String[] args) {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
        //handleStaticWebtable();
        handelingOfDynamicWebtable();
    }

    public static void handleStaticWebtable() {
        driver.get("https://www.w3schools.com/html/html_tables.asp");
        int tableRows = driver.findElements(By.xpath("//table[@class='ws-table-all']/tbody/tr")).size();
        // 1 find rows in webatable
        System.out.println("rows present in webtable : " + tableRows);

        // 2 find column present in webtable
        int tableColoumn = driver.findElements(By.xpath("//table[@class='ws-table-all']/tbody/tr/th")).size();
        System.out.println("coloumn present in webtable : " + tableColoumn);


        // 3 retrive specific row and coloumn data
        List<WebElement> rowData = driver.findElements(By.xpath("//table[@class='ws-table-all']/tbody/tr[2]/td[2]"));
        for (WebElement value : rowData) {
            System.out.println(value.getText());
        }

        // 4 retrive all the data from table
   /*    for(int r=2; r<=tableRows; r++){  // this for loop representing rows
            for(int c=1;c<=tableColoumn;c++){ // this for loop representing coloumn
String data=driver.findElement(By.xpath("//table[@class='ws-table-all']/tbody/tr["+r+"]/td["+c+"]")).getText();// note we can't passed variable directly in xpath

System.out.println("data of row :"+r+" : coloumn : "+c+"="+data);
            }
            System.out.println("data retrive :"+"--------------------row value"+r+"retrived completed--------------------");
        }
*/
        // 5 retrive data  from webtable on specific condition
        // eg print country of island trading company
        for (int r = 2; r <= tableRows; r++) {

            String company = driver.findElement(By.xpath("//table[@class='ws-table-all']/tbody/tr[" + r + "]/td[" + 1 + "]")).getText();// note we can't passed variable directly in xpath
// here in above line first we check country from coloumn 1 that is constant thats why we not taken for loop for coloumn here
            if (company.equals("Island Trading")) {
                String country = driver.findElement(By.xpath("//table[@class='ws-table-all']/tbody/tr[" + r + "]/td[" + 3 + "]")).getText();
                System.out.println("country of company island trading :" + country);
            }

        }


    }

    public static void handelingOfDynamicWebtable() {
        driver.get("https://www2.asx.com.au/markets/trade-our-cash-market/directory");
        int coloumnsize = driver.findElements(By.xpath("//table[@class='table']/thead/tr/th")).size();
/*List<WebElement> stringCo=driver.findElements(By.xpath("//table[@class='table']/thead/tr/th"));

for(WebElement ele:stringCo){
    System.out.println(ele.getText());                     we use this to print header value
}
*/

        driver.findElement(By.xpath("//button[@id='onetrust-accept-btn-handler']")).click();
        int rowsize = driver.findElements(By.xpath("//table[@class='table']/tbody/tr")).size();
        System.out.println("row size: " + rowsize);

// find total number of pages present in table
//
        WebElement lastItemArrow = driver.findElement(By.xpath("//button[normalize-space()='»']"));
        lastItemArrow.click();

        String totalNumberOfPages = driver.findElement(By.xpath("//li[@class='page-item active']")).getText();
        System.out.println("TotalNUmber of pages: " + totalNumberOfPages);
        int pageCount = Integer.valueOf(totalNumberOfPages);
        System.out.println("page Count :" + pageCount);
        // note whichever page is active it's number will be not clickable and in dom it's class change to active the page number where we click it will become active

        WebElement firstPage = driver.findElement(By.xpath("//button[normalize-space()='«']"));
        if (firstPage.isDisplayed()) {
            firstPage.click();
        } else {
            driver.navigate().refresh();
            firstPage.click();
        }


        WebElement akitivePage = null;
        for (int p = 1; p < pageCount; p++) {
            akitivePage = driver.findElement(By.xpath("//li[@class='page-item active']"));// this xpath always return aktive page count

        }
        System.out.println("aktivePage :" + akitivePage.getText());
        driver.quit();
    }


}