package SeleniumBasic.methods;

import ProjectTrial.genericMethodForDroopdown;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DroppdownHandeling {
    public static WebDriver driver;

    public static void main(String[] args) throws InterruptedException, IOException {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));

        //droppdownWithMultipleElementSelect(driver, "All");// select all value from droppdown
     //  droppdownWithMultipleElementSelect(driver, "choice 2 2","choice 2 3");//select multiple element by choice
        //droppdownWithSelectTag();
        //droppdownWithNoSelecttag();

       // droppdownSorting();
        //AutoSuggestDRoppdown();
      //  AutoCompleteGooglePlacesDropdown();
        HoverDroppdown();
    }


    public static void droppdownWithSelectTag() {

        driver.get("https://www.amazon.in/");
        driver.navigate().refresh();
        Actions act = new Actions(driver);

        WebElement selectTagDroppdown = driver.findElement(By.xpath("//select[@id='searchDropdownBox']"));
        selectTagDroppdown.click();
        Select select=new Select(selectTagDroppdown);
        // select.selectByIndex(3); // we have three method to select option from droppdown 1. selectByIndex(5);
        // select.selectByValue("search-alias=alexa-skills");// 2 selectByValue();
        // select.selectByVisibleText("Apps & Games");// 3 selectByVisibleText();
        //WebElement loadSelectOption=driver.findElement(By.xpath("//input[@id='twotabsearchtextbox']"));
        // driver.switchTo().activeElement().sendKeys(Keys.ENTER);
//      List<WebElement> allOptions= select.getOptions();
//      for(WebElement option:allOptions){
//
//          System.out.println(option.getText());
//          if(option.getText().equals("Amazon Fresh")){
//              option.click();
//              driver.switchTo().activeElement().sendKeys(Keys.ENTER);
//          }
//
//      }
        genericMethodForDroopdown.selectValueFromDroppdown(selectTagDroppdown, "Amazon Fresh");// generic method to select value from droppdown

        // we have 4 methods to deselect options from droppdown
        //select.deselectByIndex(3); // method 1 deselectByIndex();
        // select.deselectByValue("search-alias=alexa-skills");// method 2 deselectByValue();
        //select.deselectByVisibleText("Apps & Games");// method 3 deselectByVisibleText();
        //  select.deselectAll();// method 4 deselectAll();
    }

    public static void droppdownWithNoSelecttag() {

        driver.get("https://www.hdfcbank.com/");
        //Actions act=new Actions(driver);
        WebElement productTypeDroppdown = driver.findElement(By.xpath("(//a[@class='btn-primary dropdown-toggle btn-block'])[1]"));
        productTypeDroppdown.click();
        //driver.findElement(By.xpath("//li[text()='Accounts']")).click();

        List<WebElement> productType = driver.findElements(By.xpath("//ul[@class='dropdown1 dropdown-menu']/child::*"));
        System.out.println("product size: " + productType.size());
        for (WebElement ele : productType) {
            if (ele.getText().equals("Credit Cards")) {
                ele.click();
            }
        }
    }


    public static void droppdownWithMultipleElementSelect(WebDriver driver, String... values) {
// use this url for testinf this method https://www.jqueryscript.net/demo/Drop-Down-Combo-Tree/
        driver.get("https://www.jqueryscript.net/demo/Drop-Down-Combo-Tree/");
        driver.findElement(By.xpath("//input[@id='justAnInputBox']")).click();

        List<WebElement> choiceList = driver.findElements(By.xpath("//span[@class='comboTreeItemTitle']"));
        if (!values[0].equalsIgnoreCase("All")) { // we write this condition if we wan't select all values then we use this condition true
            for (WebElement item : choiceList) {
                {
                    String text = item.getText();
                    for (String val : values) {
                        if (text.equalsIgnoreCase(val)) {
                            item.click();
                            break;

                        }

                    }
                }
            }
        } else {
            for (WebElement choice : choiceList) {

                if (choice.isDisplayed()) {
                    choice.click();
                }
            }
        }
    }

    public static void droppdownSorting() {
        driver.get("https://www.twoplugs.com/");
        driver.findElement(By.xpath("//a[@href='/newsearchserviceneed']")).click();
        WebElement droppdownCap = driver.findElement(By.name("category_id"));
        Select sc = new Select(droppdownCap);
        List<WebElement> options = sc.getOptions();
        ArrayList original = new ArrayList<>();

        ArrayList temp = new ArrayList<>();
        for (WebElement element : options) {
            System.out.println(element.getText());
            original.add(element.getText());
            temp.add(element.getText());
        }
        System.out.println("original before start:"+original);
        System.out.println("temp list before start: " + temp);
        Collections.sort(temp); // this method sort the elements in temp list
        System.out.println("temp list after sort: " + temp);
        if (!original.equals(temp)) {
            System.out.println("droppdown sorted");
        } else {
            System.out.println("droppdown not sorted");
        }
    }

    public static void AutoSuggestDRoppdown() throws InterruptedException {

        driver.get("https://www.bing.com/");
        driver.findElement(By.xpath("//div[@class='cdxNudgeCloseBtn ']")).click();
        driver.findElement(By.xpath("//textarea[@id='sb_form_q']")).sendKeys("am");
        List<WebElement> droppdonOption = driver.findElements(By.xpath("//ul[@class='sa_drw' and @id='sa_ul']/li"));
        Thread.sleep(5000);
        System.out.println("size of auto suggetsion" + droppdonOption.size());
        for (WebElement element : droppdonOption) {
            System.out.println("name of comp:" + element.getText());
          driver.switchTo().activeElement().sendKeys(Keys.ARROW_DOWN);
            if (element.getText().equals("amazon mini tv")) {
                element.click();
                break;
            }
        }
    }

    public static void AutoCompleteGooglePlacesDropdown() {
        // in the droppdown whenever you enter anything it suggest the values and this values comes from api
        //  and we can't inspect them directly
        driver.get("https://www.twoplugs.com/");
        Actions act = new Actions(driver);
        driver.findElement(By.xpath("//a[@href='/newsearchserviceneed']")).click();
        WebElement droppdown = driver.findElement(By.xpath("//input[@id='autocomplete']"));
        droppdown.clear();
        droppdown.sendKeys("To");
        String text = null;

        do {
            droppdown.sendKeys(Keys.ARROW_DOWN);
            text = droppdown.getAttribute("value");
            System.out.println("values: " + text);
            if (text.equals("Togo")) {
                droppdown.sendKeys(Keys.ENTER);
                break;
            }
        } while (!text.isEmpty());

        {

        }
    }


    public static void HoverDroppdown() throws IOException {
        driver.get("https://www.flipkart.com/");
        WebElement cross = null;
        try {
            cross = driver.findElement(By.xpath("//button[@class='_2KpZ6l _2doB4z']"));
            while (cross.isDisplayed()) {
                cross.click();
            }
        } catch (Exception e) {
            System.out.println(e);


        }
        WebElement fashion = driver.findElement(By.xpath("//div[contains(text(),'Fashion')]"));
        Actions act = new Actions(driver);
        act.moveToElement(fashion).perform();

        List<WebElement> option = driver.findElements(By.xpath("//a[starts-with(@class,'_6WOcW9')]"));
        for (WebElement ele : option) {
            System.out.println(ele.getText());
            if (ele.getText().equals("Men's Raincoat")) {
                ele.click();
                break;
            }
        }

    }

}

