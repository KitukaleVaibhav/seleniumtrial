package SeleniumBasic.methods;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;
import java.util.Set;

public class HandelingOfCookies {
    static WebDriver driver;
    public static void main(String[] args) {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
        //getCookies();
        //printCookies();
     //  addCookies();
        deleteCookie();
    }

    public static void getCookies(){
        driver.get("https://www.imdb.com/");
       Set<Cookie> cookies= driver.manage().getCookies();
       System.out.println("Size of cookies :"+cookies.size());
    }
    public static void printCookies(){
        driver.get("https://www.imdb.com/");
        Set<Cookie> cookies= driver.manage().getCookies();
        System.out.println("Size of cookies :"+cookies.size());


            for (Cookie cookie : cookies) {
                System.out.println("name of cookie :"+cookie.getName()+"| Value of cookie : "+cookie.getValue());
                System.out.println("domain of cookie :"+cookie.getDomain());
                System.out.println("path cookie :"+cookie.getPath());
                System.out.println("site of cookie :"+cookie.getSameSite());
                System.out.println("expiry of cookies :"+cookie.getExpiry());
                System.out.println("------------------------------------------------------------");

            }

    }

    public static void addCookies(){
        driver.get("https://www.imdb.com/");
        // add cookie
       Cookie cookie=new Cookie("Cookie1","1234");
       driver.manage().addCookie(cookie);
        Set<Cookie> cookies= driver.manage().getCookies();
        System.out.println("Size of cookies :"+cookies.size());


        for (Cookie cookee : cookies) {
            System.out.println("name of cookie :"+cookee.getName()+"| Value of cookie : "+cookee.getValue());
            System.out.println("domain of cookie :"+cookee.getDomain());
            System.out.println("path cookie :"+cookee.getPath());
            System.out.println("site of cookie :"+cookee.getSameSite());
            System.out.println("expiry of cookies :"+cookee.getExpiry());
            System.out.println("------------------------------------------------------------");

        }

    }
    public static void deleteCookie(){
        Set<Cookie> cookies;
        driver.get("https://www.imdb.com/");
        Cookie cookie=new Cookie("Cookie2","1234#@2627");
        driver.manage().addCookie(cookie);
        cookies= driver.manage().getCookies();
        System.out.println("size of cookie before delete :"+cookies.size());
        driver.manage().deleteCookie(cookie);
        cookies= driver.manage().getCookies();
        System.out.println("size of cookie after delete :"+cookies.size());

        driver.manage().deleteCookieNamed("Cookie2");// this method use to delete cookie by cookie name
        cookies= driver.manage().getCookies();

        driver.manage().deleteAllCookies();// this method use to delete all cookies from browser
        cookies= driver.manage().getCookies();
        System.out.println("size of cookie after delete :"+cookies.size());
    }
}
