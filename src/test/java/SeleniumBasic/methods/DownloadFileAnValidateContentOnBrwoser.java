package SeleniumBasic.methods;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import java.util.ArrayList;

public class DownloadFileAnValidateContentOnBrwoser {

    @Test
    public static void test(){
        WebDriverManager.chromedriver().setup();
        WebDriver driver=new ChromeDriver();
        driver.get("https://freetestdata.com/document-files/pdf/");

        // Find the link to the PDF (this example assumes you have a direct link to the PDF)
        WebElement pdfLink = driver.findElement(By.xpath("(//span[text()='Select File & Download'])[1]"));

        // Open the PDF link in a new tab using Ctrl+Click (Cmd+Click on Mac)
        String openInNewTab = Keys.chord(Keys.CONTROL, Keys.RETURN);
        pdfLink.sendKeys(openInNewTab);

        // Alternatively, you can use Actions class to open link in new tab (using right-click options)

        // Wait for the new tab to load
        try {
            Thread.sleep(2000); // Adjust this sleep time according to your needs
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Get a list of all tab handles
        ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());

        // Switch to the new tab
        driver.switchTo().window(tabs.get(1));

        // The PDF should now be open in the new tab
    }
    }


