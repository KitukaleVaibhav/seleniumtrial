package SeleniumBasic.methods;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

public class ElemrntsDisplaySelectedEnableMethods {
    public static void main(String[] args) throws InterruptedException {


        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
        driver.get("https://www.amazon.in/");
        WebElement searchbox = driver.findElement(By.xpath("//input[@id='twotabsearchtextbox']"));
        boolean flag = searchbox.isDisplayed(); // isDisplayed method use to check is element is Displayed
        System.out.println("searchbox is displayed:" + flag);
        boolean flag2 = searchbox.isEnabled(); // isEnabled(); method use to check element is enabled
        System.out.println("searchbox is enabled:" + flag2);

        WebElement mobiles = driver.findElement(By.xpath("(//a[text()='Mobiles'])[1]"));
        mobiles.click();
        WebElement checkbox = driver.findElement(By.xpath("(//span[text()='Made for Amazon'])[2]"));
        checkbox.click();
        Thread.sleep(5000);
        try {
            boolean flag3 = checkbox.isSelected();// we use isSelected method to check any element is Selected.
            System.out.println("chekbox is Selected :" + flag3);
        } catch (Exception e) {
            e.printStackTrace();
            driver.navigate().refresh();
            boolean flag3 = checkbox.isSelected();// we use isSelected method to check any element is Selected.
            System.out.println("chekbox is Selected :" + flag3);
        }
    }}
