package SeleniumBasic.methods;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.instrument.Instrumentation;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Random;

public class ScrrenshotCapturing {

    /*
    to work with screenshot we have a special interface for this (TakesScreenshot)
    we need type caste driver because webdriver interface not extend TakesScreenshot interface so it dones't contain it's method
    so that we need to downcast Webdriver
     */
    static WebDriver driver;
    private static Instrumentation instrumentation;

    public static void main(String[] args) throws IOException {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        // captureFullScrrenshot();
        //takesScreenshotOfSpecificPortion();
        captureScrrenshotAsByteArray();
    }

    public static void captureScrrenshotAsByteArray() throws IOException {
        // String time= String.valueOf(LocalDateTime.now());
        Random ran = new Random();
        int random = ran.nextInt();
        String ranValue = Integer.toString(random);
        driver.get("https://www.imdb.com/");
        TakesScreenshot ts = (TakesScreenshot) driver;
        byte[] Sourcefile = ts.getScreenshotAs(OutputType.BYTES);
        String destinationFilePath = "C:\\Users\\HP\\IdeaProjects\\SeleniumPtoject\\Screenshot\\screenshotByte.png";

        // Write the byte array to a file
        FileOutputStream fos = new FileOutputStream(destinationFilePath);
        fos.write(Sourcefile);
        fos.flush();
        fos.close();
        System.out.println("Screenshot saved to " + destinationFilePath);


    }


    public static void captureFullScrrenshot() throws IOException {
        // String time= String.valueOf(LocalDateTime.now());
        Random ran = new Random();
        int random = ran.nextInt();
        String ranValue = Integer.toString(random);
        driver.get("https://www.imdb.com/");
        TakesScreenshot ts = (TakesScreenshot) driver;
        File Sourcefile = ts.getScreenshotAs(OutputType.FILE);
        System.out.println("size of object:" + instrumentation.getObjectSize(Sourcefile));
        File targetLocation = new File("C:\\Users\\HP\\IdeaProjects\\SeleniumPtoject\\Screenshot\\firstpage" + ranValue + ".png");
        FileUtils.copyFile(Sourcefile, targetLocation);

    }

    public static void takesScreenshotOfSpecificPortion() throws IOException {
        Random ran = new Random();
        int random = ran.nextInt();
        String ranValue = Integer.toString(random);
        driver.get("https://www.imdb.com/");
        WebElement portion = driver.findElement(By.xpath("//div[@id='inline20_responsive_wrapper']"));
        // to capture Scrrenshot of specific portion/webelement first identify those webelement and weblement have aslo getScreenshotAs method using that we can capture those webelement
        File Sourcefile = portion.getScreenshotAs(OutputType.FILE);
        File targetLocation = new File("C:\\Users\\HP\\IdeaProjects\\SeleniumPtoject\\Screenshot\\portion" + ranValue + ".png");
        FileUtils.copyFile(Sourcefile, targetLocation);
    }
}
