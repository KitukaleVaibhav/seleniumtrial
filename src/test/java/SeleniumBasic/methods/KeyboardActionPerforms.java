package SeleniumBasic.methods;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.time.Duration;

public class KeyboardActionPerforms {
    static WebDriver driver;

    public static void main(String[] args) throws InterruptedException {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
        // keyboardAction();
        keyboardAction2();
    }

    public static void keyboardAction() throws InterruptedException {
        driver.get("https://the-internet.herokuapp.com/key_presses");
        WebElement inputbox = driver.findElement(By.xpath("//input[@id='target']"));
        Actions act = new Actions(driver);
        act.sendKeys(Keys.ENTER).perform();// Keys enum have a keyboard buttons
        Thread.sleep(4000);
        act.sendKeys(Keys.ARROW_LEFT).perform();
        Thread.sleep(4000);
        act.sendKeys(Keys.ARROW_UP).perform();
        Thread.sleep(4000);
        act.sendKeys(Keys.BACK_SPACE).perform();
    }

    public static void keyboardAction2() {
        driver.get("http://textcompare.com/");
        driver.findElement(By.xpath("//textarea[@name='frm_compare_1']")).click();
        Actions act = new Actions(driver);
        // CTR+A
        act.keyDown(Keys.CONTROL);
        act.sendKeys("a");
        act.keyUp(Keys.CONTROL);
        act.perform();
        // act.sendKeys(Keys.ENTER).perform();
        //CTRL+C
        act.keyDown(Keys.CONTROL);
        act.sendKeys("c");
        act.keyUp(Keys.CONTROL);
        act.perform();


        act.sendKeys(Keys.TAB);//

        //CTRL+v

        act.keyDown(Keys.CONTROL);
        act.sendKeys("v");
        act.keyUp(Keys.CONTROL);
        act.perform();

    }
}
