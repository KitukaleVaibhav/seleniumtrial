package SeleniumBasic.methods;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.time.Duration;

public class HandleSlider {
    static WebDriver driver;

    public static void main(String[] args) {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        HandleSlider();
        //rangeSlider();
    }

    public static void HandleSlider() {
        /*
        to handle slider first we need to know initial status of slider where it present we need it's dimension first
        you need to add x-offset and y offset when value need to increase user positive x value and to decrearse use negative x value
         */
        driver.get("https://jqueryui.com/slider/#colorpicker");
        WebElement frame1 = driver.findElement(By.xpath("//iframe[@class='demo-frame']"));
        driver.switchTo().frame(frame1);
        WebElement slider = driver.findElement(By.xpath("//*[@id=\"green\"]/span"));
        System.out.println("get location :" + slider.getLocation());
        System.out.println("get size :" + slider.getSize());
        Actions act = new Actions(driver);
        act.dragAndDropBy(slider, -100, 115).perform();
    }


    public static void rangeSlider() {
        driver.get("https://jqueryui.com/slider/#range");
        WebElement iframe1 = driver.findElement(By.xpath("//iframe[@class='demo-frame']"));
        driver.switchTo().frame(iframe1);

        WebElement minSlider = driver.findElement(By.xpath("//*[@id=\"slider-range\"]/span[1]"));
        System.out.println("location of min Slider:" + minSlider.getLocation());//(84, 47) here 84 is X-offset and 47 is Y-offset get location return the x and y axes position using that we slide the slider

        System.out.println(driver.findElement(By.xpath("//input[@id='amount']")).getAttribute("value"));
        WebElement maxSlider = driver.findElement(By.xpath("//*[@id=\"slider-range\"]/span[2]"));
        System.out.println("location of max Slider:" + maxSlider.getLocation());// (341, 47)

        Actions act = new Actions(driver);
        act.dragAndDropBy(minSlider, 100, 0).perform();  // note if you don't want to slide in y axes then we can use Y-axis=0
        System.out.println("location of min Slider after moving:" + minSlider.getLocation());
        System.out.println(driver.findElement(By.xpath("//input[@id='amount']")).getAttribute("value"));
        act.dragAndDropBy(maxSlider, -100, 0).perform();
        System.out.println("location of max Slider after moving:" + maxSlider.getLocation());
        System.out.println(driver.findElement(By.xpath("//input[@id='amount']")).getAttribute("value"));
    }
}
