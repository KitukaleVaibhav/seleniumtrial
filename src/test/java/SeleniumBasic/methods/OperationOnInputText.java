package SeleniumBasic.methods;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.time.Duration;

public class OperationOnInputText {
    public static void main(String[] args) {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.get("https://www.amazon.in/");
        driver.navigate().refresh();
        Actions act = new Actions(driver);
        WebElement signInLink = driver.findElement(By.xpath("//span[text()='Hello, sign in']"));
        act.moveToElement(signInLink).build().perform();
        driver.findElement(By.xpath("//span[text()='Sign in']")).click();
        WebElement emailInputbox = driver.findElement(By.xpath("//input[@type='email']"));

        emailInputbox.sendKeys("9209022720");
        System.out.println(emailInputbox.getAttribute("value")); // getAttribute(); method is used to get value of any attribute.
        // here number is store in attribute value so we capture this attribute value
        emailInputbox.clear();// this method is used to clear text from input box.
    }
}
