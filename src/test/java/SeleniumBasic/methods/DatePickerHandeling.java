package SeleniumBasic.methods;

import allureListener.BaseClass;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DatePickerHandeling {
    /*
    Symbol                 meaning
 dd-MM-yy	                    06-03-22
dd-MM-yyyy	                    03-06-2022
MM-dd-yyyy	                    03-06-2022
yyyy-MM-dd                      2022-03-06
yyyy-MM-dd HH:mm	            2022-03-06 22:00:00
yyyy-MM-dd HH:mm.SSS	        2022-03-06 22:00:00.888
yyyy-MM-dd HH:mm.SSSZ	        2022-03-06 22:00:00.888+0300
EEEEE MMMMM yyyy HH:mm.SSSZ	    Saturday March 2022 10:00:00.760+0300
     */

    static WebDriver driver;

    public static void main(String[] args) throws ParseException {
        BaseClass bs = new BaseClass();
        driver = bs.initialize_driver();
        driver.get("https://www.hyrtutorials.com/p/calendar-practice.html");
        //selectPreviousMonthDate();
       // selectDateOfFutureMonthYear();
        SelectCurrentMonthdatePicker1();
    }


    public static void SelectCurrentMonthdatePicker1() throws ParseException {
        Calendar calendar = Calendar.getInstance();
        // String target = "10-09-2023";   format 1
        // SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy"); format 1
        String target = "10-Aug-2021"; // format 2 of date
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy"); // format 2 of date
        /*note: 10-Aug-2023 it's a date. date=10 2 charcater so we use dd ,month=August and it's 3 charcter it's standard format to use three character of month so we use MMM
        and year=2023 it's a 4 character so we use yyyy  */
        Date FormatedTargetDate = sdf.parse(target);
        calendar.setTime(FormatedTargetDate);
        int targetDate = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);




        System.out.println("targetDate:" + targetDate);
        System.out.println("targetMonth:" + month+1);// note in calendar indexing of month starts from 0 so january will be 0
        System.out.println("targeYear:" + year);
        WebElement datePicker = driver.findElement(By.xpath("//input[@id='second_date_picker']"));
        datePicker.click();
        driver.findElement(By.xpath("//table[@class='ui-datepicker-calendar']//td[not(contains(@class,'ui-datepicker-other-month'))]/a[text()=" + targetDate + "]")).click();
        // note here in above line we use a[text()="+targetDate+"]" because we need to passed dynamic value so that we add targetdate variable there
    }

    public static void selectPreviousMonthDate() throws ParseException {
        Calendar calendar = Calendar.getInstance();
        String target = "10-June-2023";
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy"); // format 2 of date
        Date FormatedTargetDate = sdf.parse(target);
        calendar.setTime(FormatedTargetDate);
        int targetDate = calendar.get(Calendar.DAY_OF_MONTH);
        int targetMonth = calendar.get(Calendar.MONTH);
        int targetYear = calendar.get(Calendar.YEAR);
        WebElement datePicker = driver.findElement(By.xpath("//input[@id='second_date_picker']"));
        datePicker.click();
        String ActualMonthYear = driver.findElement(By.xpath("//div[@class='ui-datepicker-title']")).getText(); // it will return date August 2023
        calendar.setTime(new SimpleDateFormat("MMM yyyy").parse(ActualMonthYear));// here we passing the value Actual month and year that we received through above line in string format
        // and passed in Calender to set this date format after that using this Calender we will retrive the month in int format
        int currentMonth = calendar.get(Calendar.MONTH);
        int actualYear = calendar.get(Calendar.YEAR);
        while (targetMonth < currentMonth || targetYear < actualYear) { // note when we use || or condition then it will execute till the both condition will not fail
//            WebElement previousMonthArrowButton = driver.findElement(By.xpath("//span[@class='ui-icon ui-icon-circle-triangle-w']"));
//            previousMonthArrowButton.click();
            WebElement element = driver.findElement(By.xpath("//a[@class='ui-datepicker-prev ui-corner-all']"));
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
            element.click();

            ActualMonthYear = driver.findElement(By.xpath("//div[@class='ui-datepicker-title']")).getText();
            calendar.setTime(new SimpleDateFormat("MMM yyyy").parse(ActualMonthYear));
            currentMonth = calendar.get(Calendar.MONTH); // by each time it will get new data of month
            actualYear = calendar.get(Calendar.YEAR);// bu each time it will get new data of year
        }

        WebElement finaldate=driver.findElement(By.xpath("//table[@class='ui-datepicker-calendar']//td[not(contains(@class,'ui-datepicker-other-month'))]/a[text()=" + targetDate + "]"));

    }

    public static void selectDateOfFutureMonthYear() throws ParseException {
        Calendar calendar = Calendar.getInstance();
        String target = "10-June-2025";
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy"); // format 2 of date
        Date FormatedTargetDate = sdf.parse(target);
        calendar.setTime(FormatedTargetDate);
        int targetDate = calendar.get(Calendar.DAY_OF_MONTH);
        int targetMonth = calendar.get(Calendar.MONTH);
        int targetYear = calendar.get(Calendar.YEAR);
        WebElement datePicker = driver.findElement(By.xpath("//input[@id='second_date_picker']"));
        datePicker.click();
        String ActualMonthYear = driver.findElement(By.xpath("//div[@class='ui-datepicker-title']")).getText(); // it will return date August 2023
        calendar.setTime(new SimpleDateFormat("MMM yyyy").parse(ActualMonthYear));// here we passing the value Actual month and year that we received through above line in string format
        // and passed in Calender to set this date format after that using this Calender we will retrive the month in int format
        int currentMonth = calendar.get(Calendar.MONTH);
        int actualYear = calendar.get(Calendar.YEAR);
        while (targetMonth > currentMonth || targetYear > actualYear) { // note when we use || or condition then it will execute till the both condition will not fail
            WebElement nexMonthArrowButton = driver.findElement(By.xpath("//a[@class='ui-datepicker-next ui-corner-all']"));
            nexMonthArrowButton.click();
            ActualMonthYear = driver.findElement(By.xpath("//div[@class='ui-datepicker-title']")).getText();
            calendar.setTime(new SimpleDateFormat("MMM yyyy").parse(ActualMonthYear));
            currentMonth = calendar.get(Calendar.MONTH); // by each time it will get new data of month
            actualYear = calendar.get(Calendar.YEAR);// bu each time it will get new data of year
        }
        driver.findElement(By.xpath("//table[@class='ui-datepicker-calendar']//td[not(contains(@class,'ui-datepicker-other-month'))]/a[text()=" + targetDate + "]")).click();
    }

    public static void selectDateUtilityMethod(WebDriver driver, String targetDate, String targetFormat) throws ParseException {
        Calendar calendar = Calendar.getInstance();
        String target = "10-June-2025";
        SimpleDateFormat sdf = new SimpleDateFormat(targetFormat); // format 2 of date
        sdf.setLenient(false);// The setLenient(boolean leniency) method in DateFormat class is used to specify whether the interpretation of the date and time of this DateFormat object is to be lenient or not. Parameters:
        // The method takes one parameter leniency of the boolean type that refers to the mode of the calendar of the DateFormat object.
        Date FormatedTargetDate = sdf.parse(targetDate);
        calendar.setTime(FormatedTargetDate);
        int targetDay = calendar.get(Calendar.DAY_OF_MONTH);
        int targetMonth = calendar.get(Calendar.MONTH);
        int targetYear = calendar.get(Calendar.YEAR);
        WebElement datePicker = driver.findElement(By.xpath("//input[@id='second_date_picker']"));
        datePicker.click();
        String ActualMonthYear = driver.findElement(By.xpath("//div[@class='ui-datepicker-title']")).getText(); // it will return date August 2023
        calendar.setTime(new SimpleDateFormat("MMM yyyy").parse(ActualMonthYear));// here we passing the value Actual month and year that we received through above line in string format
        // and passed in Calender to set this date format after that using this Calender we will retrive the month in int format
        int currentMonth = calendar.get(Calendar.MONTH);
        int actualYear = calendar.get(Calendar.YEAR);
        while (targetMonth > currentMonth || targetYear > actualYear) { // note when we use || or condition then it will execute till the both condition will not fail
            WebElement nexMonthArrowButton = driver.findElement(By.xpath("//a[@class='ui-datepicker-next ui-corner-all']"));
            nexMonthArrowButton.click();
            ActualMonthYear = driver.findElement(By.xpath("//div[@class='ui-datepicker-title']")).getText();
            calendar.setTime(new SimpleDateFormat("MMM yyyy").parse(ActualMonthYear));
            currentMonth = calendar.get(Calendar.MONTH); // by each time it will get new data of month
            actualYear = calendar.get(Calendar.YEAR);// bu each time it will g
        }
        driver.findElement(By.xpath("//table[@class='ui-datepicker-calendar']//td[not(contains(@class,'ui-datepicker-other-month'))]/a[text()=" + targetDate + "]")).click();
    }

    public static void setDate(WebDriver driver, String targetDate, String targetFormat) throws ParseException {
        Calendar calendar = Calendar.getInstance();
        String target = "10-June-2025";
        SimpleDateFormat sdf = new SimpleDateFormat(targetFormat); // format 2 of date
        sdf.setLenient(false);// The setLenient(boolean leniency) method in DateFormat class is used to specify whether the interpretation of the date and time of this DateFormat object is to be lenient or not. Parameters:
        // The method takes one parameter leniency of the boolean type that refers to the mode of the calendar of the DateFormat object.
        Date FormatedTargetDate = sdf.parse(targetDate);
        calendar.setTime(FormatedTargetDate);
        int targetDay = calendar.get(Calendar.DAY_OF_MONTH);
        int targetMonth = calendar.get(Calendar.MONTH);
        int targetYear = calendar.get(Calendar.YEAR);
        String ActualMonthYear = driver.findElement(By.xpath("//div[@class='ui-datepicker-title']")).getText(); // it will return date August 2023
        calendar.setTime(new SimpleDateFormat("MMM yyyy").parse(ActualMonthYear));// here we passing the value Actual month and year that we received through above line in string format
        // and passed in Calender to set this date format after that using this Calender we will retrive the month in int format
        int currentMonth = calendar.get(Calendar.MONTH);
        int actualYear = calendar.get(Calendar.YEAR);

        if (targetMonth > currentMonth) {
            while (targetMonth > currentMonth || targetYear > actualYear) { // note when we use || or condition then it will execute till the both condition will not fail
                WebElement nexMonthArrowButton = driver.findElement(By.xpath("//a[@class='ui-datepicker-next ui-corner-all']"));
                nexMonthArrowButton.click();
                ActualMonthYear = driver.findElement(By.xpath("//div[@class='ui-datepicker-title']")).getText();
                calendar.setTime(new SimpleDateFormat("MMM yyyy").parse(ActualMonthYear));
                currentMonth = calendar.get(Calendar.MONTH); // by each time it will get new data of month
                actualYear = calendar.get(Calendar.YEAR);// bu each time it will g
            }

        }else{
            while (targetMonth<currentMonth || targetYear<actualYear){
                WebElement previousMonthArrowButton = driver.findElement(By.xpath("//span[@class='ui-icon ui-icon-circle-triangle-w']"));
                previousMonthArrowButton.click();
                ActualMonthYear = driver.findElement(By.xpath("//div[@class='ui-datepicker-title']")).getText();
                calendar.setTime(new SimpleDateFormat("MMM yyyy").parse(ActualMonthYear));
                currentMonth = calendar.get(Calendar.MONTH); // by each time it will get new data of month
                actualYear = calendar.get(Calendar.YEAR);// bu each time it will get new data of year
            }
        }
        driver.findElement(By.xpath("//table[@class='ui-datepicker-calendar']//td[not(contains(@class,'ui-datepicker-other-month'))]/a[text()=" + targetDate + "]")).click();

    }}