package SeleniumBasic.methods;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.time.Duration;
import java.util.HashMap;
import java.util.Set;

public class DownloadFile {
    static WebDriver driver;

    static String location = "C:\\Users\\HP\\IdeaProjects\\SeleniumPtoject\\download";

    public static void main(String[] args) {
        HashMap preferences = new HashMap();
        preferences.put("plugins.always_open_pdf_externally", true);//this preference we add in browser to download pdf type file only we need to add this for to download pdf
        // if you not add above preference to browser then this pdf file will be open in new tab
        preferences.put("download.default_directory", location);// this preference use to download file the file in mention location
        ChromeOptions option = new ChromeOptions();
        option.setExperimentalOption("prefs", preferences);
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver(option);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        downloadFile();
        // downloadPdf();
    }

    public static void downloadFile() {
        driver.get("https://demoqa.com/upload-download");
        //driver.findElement(By.xpath("//div[@class='close_ad_desk_pop']")).click();
        WebElement download = driver.findElement(By.xpath("//a[text()='Download']"));
        download.click();


    }

    public static void downloadPdf() {
        driver.get("https://freetestdata.com/document-files/pdf/");
        driver.findElement(By.xpath("(//span[text()='Select File & Download'])[1]")).click();
    }
}