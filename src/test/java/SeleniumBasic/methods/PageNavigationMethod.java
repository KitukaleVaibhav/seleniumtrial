package SeleniumBasic.methods;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

public class PageNavigationMethod {
    public static void main(String[] args) {
        WebDriverManager.chromedriver().setup();
        WebDriver driver=new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.get("https://www.amazon.in/");

        WebElement mobiles=driver.findElement(By.xpath("(//a[text()='Mobiles'])[1]"));
        mobiles.click();
        driver.navigate().back();// this back(); method use to navigate back on the page
        driver.navigate().forward();// this forward(); method use to navigate forward on the page
        driver.navigate().refresh();// this refresh(); method use to refersh the page
        driver.navigate().to("https://www.flipkart.com/");// thi to(); method use to load the url
    }
}
