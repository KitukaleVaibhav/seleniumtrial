package SeleniumBasic.methods;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class toolTipWorking {
    //tooltip aslo called toast message
    static WebDriver driver;

    public static void main(String[] args) {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        //tooltipWithTitleAttribute();
        tooltipWithoutTitleattribute();
    }

    public static void tooltipWithTitleAttribute() {
        driver.get("https://www.automationtestinginsider.com/");
        WebElement inputbox = driver.findElement(By.xpath("//input[@name='q']"));
        Actions act = new Actions(driver);
        act.moveToElement(inputbox).perform();
        String tip = inputbox.getAttribute("Title");
        System.out.println(tip);
    }

    public static void tooltipWithoutTitleattribute() {
        /*
        if you wan't to stop tooltip in console and then inspect them from element in console tab then in
        network>source>click f8 or ctr+\ it will stop the screen and then we can inspect them from network tab
         */
        driver.get("https://demoqa.com/tool-tips");
        Actions act = new Actions(driver);
        WebElement hoverbutton = driver.findElement(By.id("toolTipButton"));
        act.moveToElement(hoverbutton).perform();
        WebElement iframe1 = driver.findElement(By.xpath("//iframe[@src='https://www.google.com/recaptcha/api2/aframe']"));
        driver.switchTo().frame(iframe1);
        String tooltip = driver.findElement(By.xpath("//div[@class='tooltip-inner']")).getText();

        System.out.println("tooltip text : " + tooltip);
    }
}
