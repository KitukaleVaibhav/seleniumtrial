package SeleniumBasic.methods;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

public class HandelingOfCheckboxes {
    static WebDriver driver;

    public static void main(String[] args) {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://itera-qa.azurewebsites.net/home/automation");// this method use to load url in browser
        // SelectSpecificCheckbox();
        selectMultipleCheckboxes();
        //selectMultipleCheckboxByChoice();
        //selectLastTwoCheckboxes();

    }

    public static void selectSpecificCheckbox() {
        WebElement chekckbox = driver.findElement(By.id("tuesday"));
        chekckbox.click();
    }

    public static void selectMultipleCheckboxes() {
        JavaScriptExecutorUse.scrollPageDown(driver);
        List<WebElement> Checkboxes = driver.findElements(By.xpath("//input[@type='checkbox' and contains(@id,'day')]"));
        System.out.println(Checkboxes.size());
        for (WebElement checkbox : Checkboxes) {
            System.out.println(checkbox.getAttribute("id"));
            checkbox.click();
        }
    }

    public static void selectMultipleCheckboxByChoice() {
        List<WebElement> Checkboxes = driver.findElements(By.xpath("//input[@type='checkbox' and contains(@id,'day')]"));
        for (WebElement checkbox : Checkboxes) {
            String CheckboxValue = checkbox.getAttribute("id");
            if (CheckboxValue.equals("sunday") || CheckboxValue.equals("tuesday"))
                checkbox.click();
        }
    }

    public static void selectLastTwoCheckboxes() {
        List<WebElement> Checkboxes = driver.findElements(By.xpath("//input[@type='checkbox' and contains(@id,'day')]"));
        int count = Checkboxes.size();
        for (int i = count - 2; i < count; i++) {
            Checkboxes.get(i).click();
        }
    }
}
