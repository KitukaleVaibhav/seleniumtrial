package SeleniumBasic.methods;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import java.time.Duration;

public class ActionsVsAction {
    static WebDriver driver;
    public static void main(String[] args) {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
    }

    /*
    action is a interface whatever method return action we can capture this value in action parameter
     */
    public static void actionInterfaceUse(){
        driver.get("https://jqueryui.com/slider/#range");
        WebElement element= driver.findElement(By.xpath("//a[text()='API Documentation']"));
        Actions act=new Actions(driver);
        Action action=act.moveToElement(element).build();
        action.perform();

    }

}
