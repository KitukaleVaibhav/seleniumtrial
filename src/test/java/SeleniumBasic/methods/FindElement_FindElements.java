package SeleniumBasic.methods;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.time.Duration;
import java.util.List;

public class FindElement_FindElements {
    public static void main(String[] args) {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.get("https://www.amazon.in/");
        driver.navigate().refresh();
        Actions act = new Actions(driver);
        WebElement mobiles = driver.findElement(By.xpath("(//a[text()='Mobiles'])[1]"));
        mobiles.click();
        // findElement(); method returns single webelements
        // System.out.println("size of the mobiles:");
        WebElement tvEntertainment = driver.findElement(By.xpath(" //a[@href='/b/?_encoding=UTF8&node=1389375031&ref_=sv_top_elec_mega_3']"));
        act.moveToElement(tvEntertainment).perform();
        //List<WebElement> stock= driver.findElements(By.xpath("(//div[@class='navFooterLinkCol navAccessibility'])[3]"));
        List<WebElement> stock = driver.findElements(By.xpath(" (//ul[@class='mm-category-list'])[3]"));


        for (WebElement ele : stock) {
            //System.out.println(ele.isDisplayed());
            System.out.println(ele.getText());
            FileHandeling.fileWriteTest(ele.getText());
        }


    }
}
