package SeleniumBasic.methods;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;

import java.time.Duration;

import static org.openqa.selenium.remote.CapabilityType.*;

public class HandleSSLCertificate {
    /*
    SSL (Secure Sockets Layer) is a standard security protocol for establishing a secure connection between the server
    and the client which is a browser.

SSL (Secure Socket Layer) Certificate ensures secure transformation of data across the server and client application using
strong encryption standard or digital signature. One has to install an SSL certificate or a code signing certificate.
     */
    static WebDriver driver;

    public static void main(String[] args) {
        WebDriverManager.chromedriver().setup();
        HandleInsecureCertsByChromeOptions();


    }

    @Test
    public static void HandleInsecureCertsByChromeOptions() {
        ChromeOptions options = new ChromeOptions();
        options.setAcceptInsecureCerts(true);
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.get("https://revoked.badssl.com");

    }

    public static void handleChromeCertsByDesiredCapabilities() {

    }

}
