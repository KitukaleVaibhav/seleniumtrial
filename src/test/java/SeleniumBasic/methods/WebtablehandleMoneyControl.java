package SeleniumBasic.methods;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;

import java.text.NumberFormat;
import java.text.ParseException;
import java.time.Duration;
import java.util.List;

public class WebtablehandleMoneyControl {
    @Test
    public void test1() {

        String formattedNumber = "79,257.97";

        try {
            NumberFormat numberFormat = NumberFormat.getInstance();
            Number number = numberFormat.parse(formattedNumber);
            double value = number.doubleValue();
            System.out.println("Parsed number: " + value);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void test2() throws ParseException {
        ChromeOptions options = new ChromeOptions();
        // Disable notifications
        options.addArguments("--disable-notifications");
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver(options);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.get("https://www.moneycontrol.com/indian-indices/sensex-4.html?tab=B");
        //driver.findElement(By.xpath("//button[text()='No thanks']")).click();
        int columnSize = driver.findElements(By.xpath("(//table[@class='mctable1'])[1]//thead/tr/th")).size();
        int rowSize = driver.findElements(By.xpath("(//table[@class='mctable1'])[1]/tbody/tr")).size();
        for (int a = 1; a <= rowSize; a++) {
            for (int b = 1; b <= columnSize; b++) {

                //String header = driver.findElement(By.xpath("(//table[@class='mctable1'])[1]/thead/tr/th[" + b + "]")).getText();
                String tableContent = driver.findElement(By.xpath("(//table[@class='mctable1'])[1]/tbody/tr[" + a + "]/td[" + b + "]")).getText();
                if (tableContent.equals("Classic")) {
                    for (int c = 2; c <= columnSize; c++) {
                       String resistance =driver.findElement(By.xpath("(//table[@class='mctable1'])[1]/thead/tr[1]/th["+c+"]")).getText();
                        String tc = driver.findElement(By.xpath("(//table[@class='mctable1'])[1]/tbody/tr[" + a + "]/td[" + c + "]")).getText();
                        //System.out.println("tc:" + tc);
                        NumberFormat numberFormat = NumberFormat.getInstance();
                        Number number = numberFormat.parse(tc);
                        double value = number.doubleValue();
                        System.out.println("ressitance:"+resistance+": " + value);
                        /*if (value> 79410) {
                            System.out.println("value fetched:" + value);
                        }*/
                    }
                }

            }

        }
    }


}
