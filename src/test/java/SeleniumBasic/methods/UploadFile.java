package SeleniumBasic.methods;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.time.Duration;


public class UploadFile {
    static WebDriver driver;

    public static void main(String[] args) throws AWTException {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
//HandleFileUploadBySendKeysMethodExa2();
//uploadFileWithAutoIt();
        //uploadFileByRobotClass();
        HandleFileUploadBySendKeysMethodExa1();
    }

    public static void HandleFileUploadBySendKeysMethodExa1() {
        driver.get("https://demoqa.com/upload-download");
        WebElement chooseFile = driver.findElement(By.xpath("//input[@id='uploadFile']"));
        chooseFile.sendKeys("C:\\Users\\HP\\Desktop\\APITestingNotes\\jsonPathExpression.docx");
// note we can use send keys method for file uploading when the choosFile/Upload button hag input tag and type='file' attribute
    }

    public static void HandleFileUploadBySendKeysMethodExa2() {
        driver.get("https://www.foundit.in/");
        WebElement uploadResume = driver.findElement(By.xpath("//i[@class='mqfihd-upload']"));
        uploadResume.click();
        driver.findElement(By.id("file-upload")).sendKeys("C:\\Users\\HP\\Desktop\\APITestingNotes\\jsonPathExpression.docx");
    }

    public static void uploadFileWithAutoIt() {

        driver.get("https://www.sodapdf.com/word-to-pdf/");
        WebElement uploadButton = driver.findElement(By.xpath("//label[normalize-space()='Choose file']"));
        uploadButton.click();
        try {
            Runtime.getRuntime().exec("C:\\Users\\HP\\Desktop\\AutoItScript\\autoItScript2.exe");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) { // this we add wait because file takes time to upload for that we sue thread.
            throw new RuntimeException(e);
        }

    }

    public static void uploadFileByRobotClass() throws AWTException {
        driver.get("https://www.ilovepdf.com/word_to_pdf");
        WebElement SelectWordFiles = driver.findElement(By.xpath("//span[normalize-space()='Select WORD files']"));
        SelectWordFiles.click();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        Robot rb = new Robot();

        StringSelection fileSelection = new StringSelection("D:\\MyPersonalDocuments\\Application Form DTDL.docx");
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(fileSelection, null);
        rb.keyPress(KeyEvent.VK_CONTROL);
        rb.keyPress(KeyEvent.VK_V);
        rb.keyRelease(KeyEvent.VK_CONTROL);
        rb.keyRelease(KeyEvent.VK_V);
        rb.keyPress(KeyEvent.VK_ENTER);
        rb.keyRelease(KeyEvent.VK_ENTER);
    }
}



