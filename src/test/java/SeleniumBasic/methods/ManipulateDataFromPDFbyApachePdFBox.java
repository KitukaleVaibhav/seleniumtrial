package SeleniumBasic.methods;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class ManipulateDataFromPDFbyApachePdFBox {
    public static void main(String[] args) throws Exception {
        ReadDataPdfLocal();
    }

    public static void ReadDataPdfLocal() throws Exception {
        File file = new File("C:\\Users\\HP\\IdeaProjects\\SeleniumPtoject\\download\\Free_Test_Data_100KB_PDF.pdf");
        FileInputStream fis = new FileInputStream(file);
        PDDocument pdf = PDDocument.load(fis);
        System.out.println(pdf.getPages().getCount());
        PDFTextStripper pdfTextStripper=new PDFTextStripper();
        //String docText=pdfTextStripper.getText(pdf);// this method get all pages data from pdf
        pdfTextStripper.setStartPage(1);// setStartPage and setEndPage method will set pages limit from which we need to get data
        pdfTextStripper.setEndPage(2);
        String text=pdfTextStripper.getText(pdf);
        text.contains("1 2 3 4");
        System.out.println(text);
        pdfTextStripper.setSortByPosition(true);

        System.out.println(text);
        pdf.close();
        fis.close();
    }
    public static void ReadDataFromPdfRemote(){

    }
}
