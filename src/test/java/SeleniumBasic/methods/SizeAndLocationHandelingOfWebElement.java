package SeleniumBasic.methods;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

public class SizeAndLocationHandelingOfWebElement {
    static WebDriver driver;
    public static void main(String[] args) {
        WebDriverManager.chromedriver().setup();
        driver=new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        checkSizeOfWebElement();
    }

    public static void checkSizeOfWebElement(){
        driver.get("https://www.selenium.dev/");
        WebElement selenium= driver.findElement(By.xpath("//div[@class='row']//div[1]//div[1]//div[1]//div[1]//span[1]//*[name()='svg']//*[name()='path']"));
     // Location method 1
        System.out.println("element location : "+selenium.getLocation());
        System.out.println("x-location : "+selenium.getLocation().getX());//return element distance on x-axis
        System.out.println("y-location :"+selenium.getLocation().getY());//return element distance on y-axis
        System.out.println("size of webelement :"+selenium.getSize());//  this method return width and height

        // Location method 2
        System.out.println("height : "+selenium.getRect().height);
        System.out.println("width :"+selenium.getRect().width);
        System.out.println("x-location by method 2 : "+selenium.getRect().getX());//return element distance on x-axis
        System.out.println("y-location by method 2 : "+selenium.getRect().getY());//return element distance on y-axis
        System.out.println(" Dimension: "+selenium.getRect().getDimension());//this method return width and height

    }

}
