package SeleniumBasic.methods;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class PageRelatedMethod {
    public static void main(String[] args) {

        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.amazon.in/");// this method use to load url in browser
        String PageTitle = driver.getTitle();// this method use to capture current page title
        System.out.println("page title :" + PageTitle);

        String currentPageurl = driver.getCurrentUrl();// this method use to get current page url.
        System.out.println("page url :" + currentPageurl);
        int height = driver.manage().window().getSize().getHeight();
        System.out.println("page height :" + height);
        int width = driver.manage().window().getSize().getWidth();
        System.out.println("page width :" + width);

        String pageSource = driver.getPageSource();
        System.out.println("page source: " + pageSource);
    }
}
