package SeleniumBasic.methods;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.Random;

public class PractiseJavaScriptExecutorMethod {
    static WebDriver driver;

    public static void main(String[] args) throws IOException {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        //executeBordertoElement();
        //  captureTitleByJs();
        //clickOnWebelement();
        // applyAlert();
        //  refreshPage();
        // pageScrollDownUp();
       // zoomPage();
        flashTheElement();

    }

    public static void executeBordertoElement() throws IOException {
        // this method drawing border and capturing screenshot
        Random ran = new Random();
        int random = ran.nextInt();
        String ranValue = Integer.toString(random);
        driver.get("https://www.imdb.com/");
        WebElement portion = driver.findElement(By.xpath("//div[@id='inline20_responsive_wrapper']"));
        //WebElement portion= driver.findElement(By.id("home_img_holder"));
        JavaScriptExecutorUse.drawBorderToElement(driver, portion);
        // to capture Scrrenshot of specific portion/webelement first identify those webelement and weblement have aslo getScreenshotAs method using that we can capture those webelement
        File Sourcefile = portion.getScreenshotAs(OutputType.FILE);
        File targetLocation = new File("C:\\Users\\HP\\IdeaProjects\\SeleniumPtoject\\Screenshot\\portion" + ranValue + ".png");
        FileUtils.copyFile(Sourcefile, targetLocation);

    }

    public static void captureTitleByJs() {
        driver.get("https://www.imdb.com/");
        String pageTitle = JavaScriptExecutorUse.getTitleByJS(driver);
        System.out.println("page title : " + pageTitle);
    }

    public static void clickOnWebelement() {
        driver.get("https://www.imdb.com/");
        WebElement element1 = driver.findElement(By.xpath("(//a[@role='button'])[1]"));
        JavaScriptExecutorUse.clickElementByJS(driver, element1);
    }

    public static void applyAlert() {
        driver.get("https://www.imdb.com/");
        JavaScriptExecutorUse.generateAlert(driver, "this is not correct page");

    }

    public static void refreshPage() {
        driver.get("https://www.imdb.com/");
        WebElement element1 = driver.findElement(By.xpath("(//a[@role='button'])[1]"));
        JavaScriptExecutorUse.clickElementByJS(driver, element1);
        JavaScriptExecutorUse.refreshPage(driver);
    }

    public static void pageScrollDownUp() {
        driver.get("https://www.imdb.com/");
        JavaScriptExecutorUse.scrollPageDown(driver);
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        JavaScriptExecutorUse.scrollPageUp(driver);
    }

    public static void zoomPage() {
        driver.get("https://www.imdb.com/");
        JavaScriptExecutorUse.zoomPageByJs(driver);
    }
    public static void flashTheElement(){
        driver.get("https://www.imdb.com/");
        WebElement portion = driver.findElement(By.xpath("//a[@href='/list/watchlist?ref_=nv_usr_wl_all_0']"));
        JavaScriptExecutorUse.flash(portion,driver);
    }
}
