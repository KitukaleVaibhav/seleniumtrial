package SeleniumBasic.methods;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

//note driver always point to current window
// if you wan't to perform any action on different window you need to first switch to that window
public class HandelingOfBrowserWindow {

    static WebDriver driver;

    public static void main(String[] args) {
        WebDriverManager.chromedriver().clearDriverCache().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.mahadiscom.in/en/home/");
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
//        driver.navigate().refresh();
        driver.findElement(By.xpath("//button[@class='pum-close popmake-close']")).click();
        HandleWindow();
        //closeSpecificBrowserWindow();
    }

    public static void HandleWindow() {
        WebElement facebook = driver.findElement(By.xpath("//a[@href='https://www.facebook.com/MSEDCL/']"));
        WebElement twitter = driver.findElement(By.xpath("//a[@href='https://twitter.com/MSEDCL']"));
        WebElement youtube = driver.findElement(By.xpath("//a[@href='https://m.youtube.com/channel/UC8uLzqt4vZZwVeWLD9j4CDw']"));
        WebElement instagram = driver.findElement(By.xpath("//a[@href='https://instagram.com/msedclofficial?utm_medium=copy_link']"));
        twitter.click();


        String idCurrentWindow = driver.getWindowHandle();// this method return the id of current window and it's return type is string
        //System.out.println("Current WindowId:"+idCurrentWindow);
        Set<String> windowsId = driver.getWindowHandles();// this method return the id of all windows and it's return tye is Se<String>

        // Retrive window id using iterator

        Iterator<String> ele = windowsId.iterator();
        String parentID = ele.next();
        String ChildId = ele.next();
        System.out.println("parentId: " + parentID);
        System.out.println("ChildId: " + ChildId);


        // retrive winow id using List collection

        List<String> wins = new ArrayList<String>(windowsId);// converting set to List
        String parId = wins.get(0);
        String chiId = wins.get(1);
//        System.out.println("parId: "+parId);
//        System.out.println("ChiId: "+chiId);
        //   driver.findElement(By.xpath("//span[text()='Not now']")).click();
        driver.switchTo().window(parId);// this method is used to switch between window using window id
        facebook.click();
        driver.switchTo().window(parId);
        youtube.click();
        driver.switchTo().window(parId);
        instagram.click();

        Set<String> windowsId2 = driver.getWindowHandles();

        for (String id2 : windowsId2) {
            String title = driver.switchTo().window(id2).getTitle();
            System.out.println("Window Title: " + title);

            if (title.equals("Maharashtra State Electricity Distribution Co. Ltd. | Facebook")) {
                driver.findElement(By.xpath("//span[text()='Create new account']")).click();

            }


        }
        Set<String> windowsId3 = driver.getWindowHandles();
        String destTitle = "Sign up for Facebook | Facebook";
        GenericMethods.switchWindow(windowsId3, driver, destTitle);
        driver.findElement(By.xpath("(//input[@class='_8esa'])[1]")).click();
    }

    public static void closeSpecificBrowserWindow() {
        WebElement facebook = driver.findElement(By.xpath("//a[@href='https://www.facebook.com/MSEDCL/']"));
        WebElement twitter = driver.findElement(By.xpath("//a[@href='https://twitter.com/MSEDCL']"));
        WebElement youtube = driver.findElement(By.xpath("//a[@href='https://m.youtube.com/channel/UC8uLzqt4vZZwVeWLD9j4CDw']"));
        WebElement instagram = driver.findElement(By.xpath("//a[@href='https://instagram.com/msedclofficial?utm_medium=copy_link']"));
        twitter.click();


        Set<String> windowsId = driver.getWindowHandles();// this method return the id of all windows and it's return tye is Se<String>

        // retrive winow id using List collection

        List<String> wins = new ArrayList<String>(windowsId);// converting set to List
        String parId = wins.get(0);
        String chiId = wins.get(1);

        driver.switchTo().window(parId);// this method is used to switch between window using window id
        facebook.click();
        driver.switchTo().window(parId);
        youtube.click();
        driver.switchTo().window(parId);
        instagram.click();

        Set<String> windowsId2 = driver.getWindowHandles();

        for (String id2 : windowsId2) {
            String title = driver.switchTo().window(id2).getTitle();
            System.out.println("Window Title: " + title);

            if (title.equals("Maharashtra State Electricity Distribution Co. Ltd (@MSEDCL) / Twitter") || title.equals("महावितरण - YouTube") || title.equals("Page not found • Instagram")) {
                driver.close();
            }
        }

    }
}
