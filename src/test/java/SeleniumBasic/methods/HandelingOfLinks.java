package SeleniumBasic.methods;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.time.Duration;
import java.util.List;

/*
"A broken link is a web-page that can’t be found or accessed by a user,
 for various reasons. Web servers will often return an error message when a user tries to access a broken link.
 Broken links are also often known as “dead links” or “link rots.”

 404 Page Not Found: the page/resource doesn’t exist on the server
400 Bad Request: the host server cannot understand the URL on your page
Bad host: Invalid host name: the server with that name doesn’t exist or is unreachable
Bad URL: Malformed URL (e.g. a missing bracket, extra slashes, wrong protocol, etc.)
Bad Code: Invalid HTTP response code: the server response violates HTTP spec
Empty: the host server returns “empty” responses with no content and no response code
Timeout: Timeout: HTTP requests constantly timed out during the link check
Reset: the host server drops connections. It is either misconfigured or too busy.
 */

public class HandelingOfLinks {
    static WebDriver driver;

    public static void main(String[] args) throws IOException {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        locateLinks();
        //captureBrokenLinks();
       // captureAmazonBrokenLinks();
    }


    public static void locateLinks() {
        driver.get("https://www.amazon.in/");
        // all links have anchor tag a using that we locate links on webpage
//        driver.findElement(By.linkText("New Releases")).click();// this method use to find link
//        driver.findElement(By.partialLinkText("New Releases")).click();// this method is use to find link

        // capture all links of webpage
        List<WebElement> links = driver.findElements(By.tagName("a"));
        System.out.println("links count: " + links.size());

        for (WebElement link : links) {
            //System.out.println(link.getText()); it is used to capture linktext
            System.out.println(link.getAttribute("href")+" : "+link.getText());// this used to capture link
            String content = link.getText();
            if (content.equals("Hello, sign in")) {
                link.click();
                System.out.println("succesfully click");
                break;
            }
        }
    }

    public static void captureAmazonBrokenLinks(){
        driver.get("https://www.amazon.in/");
        // all links have anchor tag a using that we locate links on webpage
//        driver.findElement(By.linkText("New Releases")).click();// this method use to find link
//        driver.findElement(By.partialLinkText("New Releases")).click();// this method is use to find link

        // capture all links of webpage
        List<WebElement> links = driver.findElements(By.tagName("a"));
        System.out.println("links count: " + links.size());
        int brokenlinks = 0;

        for (WebElement link : links) {
            String url = link.getAttribute("href");
            if (url == null || url.isEmpty()) {
                System.out.println("link is empty");
                continue;
            }try {
                URL linkk = new URL(url);// caste string format url to url we use URL class for that
                HttpURLConnection httpURLConnection = (HttpURLConnection) linkk.openConnection();
                httpURLConnection.connect();
                if (httpURLConnection.getResponseCode() >= 400) {
                    System.out.println(httpURLConnection.getResponseCode() + " :broeknlink " + linkk);
                    brokenlinks++;

                } else {
                    System.out.println(httpURLConnection.getResponseCode() + "this is normal link : " + linkk);
                }
            } catch (Exception e) {
                System.out.println(e);
            }

        }
        System.out.println("count of broken links: " + brokenlinks);
    }




    public static void captureBrokenLinks() throws IOException {
        driver.get("http://www.deadlinkcity.com/");
        int brokenlinks = 0;
        List<WebElement> links = driver.findElements(By.tagName("a"));
        for (WebElement link : links) {
            //System.out.println(link.getText());// it is used to capture linktext
            // System.out.println(link.getAttribute("href"));// this used to capture link
            String url = link.getAttribute("href");
            if (url == null || url.isEmpty()) {
                System.out.println("link is empty");
                continue;
            }
            try {
                URL linkk = new URL(url);// caste string format url to url we use URL class for that
                HttpURLConnection httpURLConnection = (HttpURLConnection) linkk.openConnection();
                httpURLConnection.connect();
                if (httpURLConnection.getResponseCode() >= 400) {
                    System.out.println(httpURLConnection.getResponseCode() + " :broeknlink " + linkk);
                    brokenlinks++;

                } else {
                    System.out.println(httpURLConnection.getResponseCode() + "this is normal link : " + linkk);
                }
            } catch (Exception e) {
                System.out.println(e);
            }

        }
        System.out.println("count of broken links: " + brokenlinks);
    }
}