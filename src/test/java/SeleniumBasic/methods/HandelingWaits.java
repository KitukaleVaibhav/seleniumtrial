package SeleniumBasic.methods;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.function.Function;

/*
selenium webdriver prvide you 3 diffrent types of wait 1.implicit wait 2. explicit wait 3. fluent wait
 */
public class HandelingWaits {
    static WebDriver driver;

    public static void main(String[] args) {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        explicitWait();

    }

    public static void implicitWait() {
        /*
        it is used to define time(period) until when webdriver should wait before throwing exception means it wait for time we mention.
        implicit wait aslo called dynamic wait because if you applied wait 15 secconds and element found in 5 seconds then it will not spend
          remaining 10 seconds it moves to next element. polling time of implicit=500ms it will check element on webpage in every 500ms
           note: when we applied implict wait from the next line it will applied on every element
                 */
        driver.get("https://www.amazon.in/");
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));// this is the syntax of implict wait
    }

    public static void explicitWait() {
        driver.get("https://www.amazon.in/");
        /*
        explicit wait can be set for specific elements
        to apply this explicit wait we have WebDriverWait class it contains various methods for various conditions.
        default polling time of explicit wait is 500ms
         */
        WebDriverWait webDriverWait = new WebDriverWait(driver, Duration.ofSeconds(10));
        WebElement element = webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@href='/gp/bestsellers/?ref_=nav_cs_bestsellers']")));
// note: if the Expected conditions match element visible then it will return the WebElement
        System.out.println("element displayed : " + element.isDisplayed());
    }

    public static void fluentWait() {
/*
FluentWait it is a class in selenium. it is same like WebDriverWait but with more flexibility in polling time and exception handeling
 */
//Wait wait=new FluentWait(driver)
//        .withTimeout(Duration.ofSeconds(30))
//        .pollingEvery(Duration.ofSeconds(5))
//        .ignoring(NoSuchElementException.class);
//
//        WebElement foo = wait.until(new Function<WebDriver,WebElement>() {
//            @Override
//            public WebElement apply(WebDriver webDriver) {
//            return driver.findElement(By.id("foo"));
//
//        )
//            }};
    }
}