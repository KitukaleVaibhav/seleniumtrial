package SeleniumBasic.interviewQuestions;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import java.time.Duration;

public class ImplementationOfSelenium implements p1.p2 {

    public static void main(String[] args) {
        ImplementationOfSelenium im=new ImplementationOfSelenium();
        im.test3();
        im.test4();
    }
    @Override
    public void test3() {
        System.out.println("test3");
    }

    @Override
    public void test4() {
        System.out.println("test3");
    }
}
interface p1 {
    void t1();

    int t2();

    interface p2 {
        void test3();

        void test4();
    }

    interface p3 {
        void test5();

        void test6();
    }
}