package SeleniumBasic.interviewQuestions;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

public class FindTrendingYoutubeVideo {
    public static void main(String[] args) throws InterruptedException {

        WebDriverManager.chromedriver().setup();
        ChromeDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.manage().window().maximize();
        driver.get("https://www.youtube.com/feed/trending?bp=6gQJRkVleHBsb3Jl");
        driver.findElement(By.xpath(" //ytd-video-renderer[@class='style-scope ytd-expanded-shelf-contents-renderer'][1]")).click();
        Thread.sleep(5000);
        driver.findElement(By.xpath("//video[@class='video-stream html5-main-video']")).click();
        Thread.sleep(5000);
        String timeAfterStop = driver.findElement(By.xpath("//span[@class='ytp-time-current']")).getText();
        System.out.println("timeAfterstop:" + timeAfterStop);

        driver.findElement(By.xpath("//video[@class='video-stream html5-main-video']")).click();
        Thread.sleep(5000);
        String timeAfterPlay = driver.findElement(By.xpath("//span[@class='ytp-time-current']")).getText();
        System.out.println("timeAfterPlay:" + timeAfterPlay);
    }
}
