package SeleniumBasic.interviewQuestions;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.checkerframework.checker.units.qual.C;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DatePickerHandling {
    @Test
    public void testDate() {

        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.get("https://www.makemytrip.com/flights/?gclid=Cj0KCQjwzva1BhD3ARIsADQuPnVUPaIvHiKfB0QqlqI1h6uwuWhzuHafV1qWTyrayDUBWmLHlefLC0YaAq09EALw_wcB&cmp=SEM|D|DF|G|Generic|Generic-Generic_DT|B_M_Makemytrip_Variants|Brand-Variants-Exact-2|RSA|Regular|V2|622487392958&s_kwcid=AL!1631!3!622487392958!e!!g!!mmt&ef_id=Cj0KCQjwzva1BhD3ARIsADQuPnVUPaIvHiKfB0QqlqI1h6uwuWhzuHafV1qWTyrayDUBWmLHlefLC0YaAq09EALw_wcB:G:s&gad_source=1");
        driver.findElement(By.xpath("//span[@class='commonModal__close']")).click();
        driver.findElement(By.xpath("//label[@for='departure']")).click();
        List<WebElement> elements = driver.findElements(By.xpath("(//div[contains(@class,'DayPicker-Day DayPicker-Day--disabled')])/child::*/p[1]"));
        for (WebElement element : elements) {
            System.out.println(element.getText());
        }
    }

    @Test
    public void test2() throws ParseException {
        String monthYear = "September 2024";
        SimpleDateFormat df = new SimpleDateFormat("MMMM yyyy");
        Date date = df.parse(monthYear);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int month = calendar.get(Calendar.MONTH+1);
        int year = calendar.get(Calendar.YEAR);
        System.out.println(month);
        System.out.println(year);

        if(month==9&&year==2024 ||month==month+1&&year==2024){

        }
    }
}
