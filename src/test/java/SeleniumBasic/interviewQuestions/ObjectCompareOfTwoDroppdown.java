package SeleniumBasic.interviewQuestions;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ObjectCompareOfTwoDroppdown {

    @Test
    public void TestObjectCompare() {
        Set<String> s1 = new HashSet<>();
        Set<String> s2 = new HashSet<>();

        s1.add("a");
        s1.add("b");
        s1.add("c");

        s2.add("a");
        s2.add("b");
        s2.add("d");
        if (s1.size() == s2.size()) {

            System.out.println("check result:" + s1.equals(s2));
        }
    }

    @Test
    public void compareList() {
        List<String> L1 = new ArrayList<>();
        List<String> L2 = new ArrayList<>();
        L1.add("d");
        L1.add("s");
        L1.add("k");
        L2.add("d");
        L2.add("s");
        L2.add("k");

        if(L1.size()== L2.size()){
            System.out.println(L1.equals(L2));
        }
    }
}
