package ProjectTrial;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class genericMethodForDroopdown {

    public static void selectValueFromDroppdown(WebElement element, String value){

        Select select=new Select(element);
        List<WebElement> allOptions= select.getOptions();
        for(WebElement option:allOptions){

            System.out.println(option.getText());
            if(option.getText().equals(value)){
                option.click();

            }

        }
    }
}
