package allureListener;

import io.qameta.allure.*;
import org.testng.annotations.Test;

import java.awt.*;

public class AllureAnnotation {

    @Test
    @Description("this test is start of the suite")
    public void test1() {
        System.out.println("test 1");
    }

    @Test
    @Severity(SeverityLevel.BLOCKER)
    public void test2() {
        System.out.println("test 2");
    }

    @Test
    @Epic("Test Epic")
    public void test3() {
        System.out.println("test 3");
    }

    @Test
    @Feature("sprint 23.12 feature")
    public void test4() {
        System.out.println("test 4");
    }

    @Test
    @Story("Testing story")
    public void test5() {
        System.out.println("test 5");
    }

    @Test
    @Step("step 1")
    public void test6() {
        System.out.println("test 6");
    }
}
