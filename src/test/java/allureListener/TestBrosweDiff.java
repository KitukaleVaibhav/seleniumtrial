package allureListener;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class TestBrosweDiff {
    public WebDriver driver=BaseClass.getDriver();


    @Test
    @Parameters("browserName")
    public void verifyAmazon(String Name){
//        BaseClass2 bs=new BaseClass2();
//        driver= bs.initialize(Name);// this method return WebDriver instance
        driver.get("https://www.amazon.in/");// this method use to load url in browser
        String PageTitle=  driver.getTitle();// this method use to capture current page title
        System.out.println("page title :"+PageTitle);
        Assert.assertEquals(PageTitle,"amazon");
    }
}
