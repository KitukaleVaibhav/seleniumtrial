package allureListener;

import io.qameta.allure.Attachment;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.io.File;

import static SeleniumBasic.methods.DroppdownHandeling.driver;

public class Listner1 implements ITestListener {
    private static String getTestMethodName(ITestResult iTestResult) {
        return iTestResult.getMethod().getConstructorOrMethod().getName();
    }

    @Attachment // it's a allure annotation
    public byte[] savesFailureScreenShot(WebDriver driver) {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }


    @Attachment(value = "{0}", type = "text/plain")
    public static String saveTextLog(String message) {
        return message;
    }

    @Override                  // CRTL+O use to add interface method in intellije
    public void onTestFailure(ITestResult result) {
        //Object testClass = result.getInstance();
        WebDriver driver = BaseClass.getDriver();
        if (driver instanceof WebDriver) {
            savesFailureScreenShot(driver);
        }
    }
}
